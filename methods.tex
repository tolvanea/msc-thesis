\chapter{Path integral Monte Carlo method}
\label{ch:PIMC}
In this section we show how imaginary-time path integrals are evaluated in practice with the Path integral Monte Carlo method (PIMC). In principle, the integrals comprise an infinite number of paths to evaluate. Fortunately, a finite number of random samples can be used to estimate the integrals at a decent and controllable accuracy. A path can be approximated with discrete line segments, and sum over all possible paths can be estimated with a finite number of Monte Carlo path samples~\cite[p.~55]{tiihonenThermalEffectsAtomic2019}. Also, random sampling can be made more efficient by importance sampling, which increases the significance of the samples by drawing them from the vicinity of the classical path.

\section{Monte Carlo method}
The path integral is essentially a high-dimensional integral. Common grid integration methods are insufficient for the task, and so, the integral is evaluated with Monte Carlo importance sampling. Let us write an integrand $g(\vec{x})$ with a chosen probability distribution $P(\vec{x})$ as
\begin{align}
    \int g(\vec{x}) \dd \vec{x} &= \int P(\vec{x}) \frac{g(\vec{x})}{P(\vec{x})}  \dd \vec{x}.
\end{align}
This integral is then interpreted as the expectation value~\cite[p.~92]{robertMonteCarloStatistical2004}\cite{kylanpaaComputationalPhysicsLecture2019}
\begin{align}
    \int g(\vec{x}) \dd \vec{x}
    &= \expval{\frac{g(\vec{x})}{P(\vec{x})}}_P\\
    &\approx \frac{1}{N} \sum_{i=1}^{N} \frac{g(\vec{x}_i)}{P(\vec{x}_i)},
\end{align}
where $\ev{\cdot}_P$ denotes that a variable $\vec{x}$ is distributed by $P$, and where samples $\vec{x}_i$ are also distributed by the $P$. The probability distribution $P$ must be such a function that can generate samples $\vec{x}_i$ efficiently. The closer the fraction $\frac{g}{P}$ is to a constant, the more accurate the discrete sum is. Therefore, a good choice of $P$ is essential for sampling efficiency.

% The probability distribution $P$ should be chosen so that the most paths are sampled near classical path.



\section{Markov chain Monte Carlo}
In essence, PIMC evaluates the high-dimensional integrals given in equation~\ref{eq:discretizedExpValOverPath}. One sample of Monte Carlo is a trajectory of all particles~\cite[p.~62]{tiihonenThermalEffectsAtomic2019}. However, there is a slight challenge in calculation of expectation value~\ref{eq:exValOpOp}, because it depends on partition the function $Z$, which by itself is a laborious path integral. This is where the \textit{Markov chain Monte Carlo}~\cite[pp.~268--271]{robertMonteCarloStatistical2004} is useful, as it brings relatively efficient sampling in high dimensions, while it also does not require normalization of the partition function.

The markov chain Monte Carlo draws random samples $\vec{x}_{i+1}$ by using information from the previously drawn sample $\vec{x}_{i}$, which forms a random walker. Consecutive steps are highly correlated, and so samples can be separated with multiple steps. The sampling is made efficient by favouring those steps that increase the probability of walker $P(\vec{x}_i)$.  The walker should be given enough time to converge to the equilibrium of the probability distribution, and the non-converged samples should be discarded. If equilibrium convergence is not established, initial walker position may form a bias in sample mean.

Even though PIMC is an exact method in theory, the insufficient number of walker steps may render a bias in practice. Given simulation time should be always long enough for random walker to find the equilibrium. If the applied simulation time is short, the true equilibrium may be hard to tell apart from a local minimum. This is larger concern with complex systems, because they require high number of samples, which is heavy on computational resources. Generally speaking, the relative computational cost of a PIMC simulation increases by the following factors:
\begin{itemize}
    \item particle count,
    \item low temperature,
    \item short time step $\Delta \tau$,
    \item heavy nucleus, and
    \item indistinguishability of electrons.
\end{itemize}
The last factor, indistinguishability of electrons, is not a concern in this work.

\section{Estimation of statistical error}
Integrated quantities are associated with a statistical uncertainty due to the finite number of random samples. The margin of error is usually presented with the \textit{standard error of the mean} (SEM), which for uncorrelated samples is
\begin{align}
    \mathrm{SEM} = \frac{\sigma}{\sqrt{N}},
\end{align}
where $\sigma$ is the standard deviation of the samples. However, if the subsequent samples are correlated, this underestimates the error. The SEM can be adjusted to take sample correlation into account with
\begin{align}
    \mathrm{SEM} = \frac{\sigma}{\sqrt{N_\mathrm{eff}}},
\end{align}
where $N_\mathrm{eff}$ is the effective sample size. It can be estimated with~\cite[p.500]{robertMonteCarloStatistical2004}
\begin{align}
    N_\mathrm{eff} = N \pa{1 + 2\sum_{n=1}^N C_n}^{-1}
\end{align}
where $C_n$ is the autocorrelation of the subsequent samples. The autocorrelation is~\cite{kylanpaaFirstprinciplesFiniteTemperature2011}
\begin{align}
    C_n = \frac{1}{\pa{N-n}\sigma^2}\sum_{i=1}^{N-n}\Big(\big(f_i - \ev{f}\big)\big(f_{i+n} - \ev{f}\big)\Big),
    %\frac{1}{N} \sum_{i=1}^{N} f(\vec{x}_i)
\end{align}
where $f_i = \frac{g(\vec{x}_i)}{P(\vec{x}_i)}$ are the samples.
All margins of error in this work correspond to 2$\;$SEM confidence.


%For more information about PIMC sampling and Metropolis algorithm, see~\cite{kylanpaaFirstprinciplesFiniteTemperature2011}~and\cite{tiihonenThermalEffectsAtomic2019}.


\section{Metropolis--Hastings algorithm}
The Markov chain Monte Carlo is most commonly utilized with some variation of a \textit{Metropolis--Hastings algorithm}. The Metropolis--Hastings algorithm implements a walker that samples the equilibrium by fulfilling the conditions of \textit{detailed balance}. In detailed balance, transitions rates between states are equal to both directions, and so the population of states does not change. That is,~\cite{tiihonenThermalEffectsAtomic2019}
\begin{align}
    P(\vec{x}_i) P(\vec{x}_i \rightarrow \vec{x}_{i+1}) = P(\vec{x}_{i+1}) \Pr(\vec{x}_{i+1} \rightarrow \vec{x}_i),\label{eq:detailedBalance}
\end{align}
where $P(\vec{x}_i)$ is the probability of state $\vec{x}_i$, and $P(\vec{x}_i \rightarrow \vec{x}_{i+1})$ is the probability of transition $\vec{x}_{i} \rightarrow \vec{x}_{i+1}$. The detailed balance is maintained by splitting the step generation into two parts. First, a move candidate $\vec{x}_{i+1}$ is generated, and then it is accepted with a certain probability. If the move is accepted, then $\vec{x}_{i+1}$ becomes the value of the next step. If it is not accepted, then the old state is chosen as the value of the next step, meaning $\vec{x}_{i+1} = \vec{x}_{i}$. The transition probability can be written
\begin{align}
    P(\vec{x}_i \rightarrow \vec{x}_{i+1}) = T(\vec{x}_i \rightarrow \vec{x}_{i+1})A(\vec{x}_i \rightarrow \vec{x}_{i+1})\label{eq:PasTA}
\end{align}
where $T(\vec{x}_i \rightarrow \vec{x}_{i+1})$ is the probability of the proposal and $A(\vec{x}_i \rightarrow \vec{x}_{i+1})$ is the probability of acceptance. Plugging~\ref{eq:PasTA} to~\ref{eq:detailedBalance}, one can find that
% \begin{align}
%      P(\vec{x}_i) T(\vec{x}_i \rightarrow \vec{x}_{i+1}) A(\vec{x}_i \rightarrow \vec{x}_{i+1}) = P(\vec{x}_{i+1}) T(\vec{x}_{i+1} \rightarrow \vec{x}_i) A(\vec{x}_{i+1} \rightarrow \vec{x}_i),
% \end{align}
%which is maintained by using
\begin{align}
    A(\vec{x}_i \rightarrow \vec{x}_{i+1}) = \min \pa{1,\; q(\vec{x}_i \rightarrow \vec{x}_{i+1})}\label{eq:acceptProb}
\end{align}
is a valid acceptance probability, where unconstrained acceptance probability is
\begin{align}
    q(\vec{x}_i \rightarrow \vec{x}_{i+1}) = \frac{T(\vec{x}_{i+1} \rightarrow \vec{x}_i) P(\vec{x}_{i+1})}{T(\vec{x}_i \rightarrow \vec{x}_{i+1}) P(\vec{x}_i)}. \label{eq:uncutAcceptProb}
\end{align}
So, in essence, the Metropolis--Hastings algorithm generates samples from the equilibrium by accepting new steps with the probability~\ref{eq:acceptProb}. If $q \leq 1$, the move is always accepted, which favours states of high probability. Note that the probability distribution $P(\vec{x}_i)$ needs not be normalized to evaluate the fraction in $q$.


\section{Path sampling}
In PIMC calculations, the state $\vec{x}_{i}$ corresponds to an imaginary-time trajectory $\RR'(\tau) \approx [\RR_1,\ldots\RR_M]$. The probability of state $P(\vec{x}_i)$ is obtained by interpreting the probability amplitude $e^{- S'[\RR'(\tau)]}$ as a Boltzmann's coefficient $e^{-\beta E^\mathrm{eff}[\RR'(\tau)]}$, where $E^\mathrm{eff}$ is an effective potential energy of the trajectory~\cite{shumwayPathIntegralMonte2000}. By favouring steps that decrease the action $S'[\RR'(\tau)]$, the walker is brought to the vicinity of the classical path.

The probability amplitude from equation~\ref{eq:rhoWithPI} can be written
\begin{align}
    \mathcal{\NC} e^{- S'[\RR'(\tau)]}
    &= \lim_{M \to \infty} \prod_{m=0}^{M-1} \NC e^{- S'(\RR_{m}, \RR_{m+1}, \Delta\tau)}.\label{eq:probAmplitudeSampling}
\end{align}
The term $S'(\RR_{m}, \RR_{m+1}, \Delta\tau)$ can be expressed similarly as in equation~\ref{eq:rhoTauExpanded}
\begin{align}
    S'(\RR_{m}, \RR_{m+1}, \Delta\tau)
    &= - \Delta \tau \; L\pa{\frac{\RR_{m+1} - \RR_{m}}{-\ii \hbar \Delta \tau}, \RR_{m}}\\
    &= \Delta \tau \pa{
            \sum_n^N \frac{m_n}{2\hbar^2}
            \frac{\pa{\vec{r}_{n,m+1} - \vec{r}_{n,m}}^2}{ \Delta\tau^2}
            + \sum_{n'>n}^N V\pa{\vec{r}_{n,m}, \vec{r}_{n',m}}
        }\\
    &= \Delta \tau \pa{
        \sum_n^N \mathcal{K}_{n,m}
        + \sum_{n'>n}^N V_{n,n',m}.\label{eq:EffWithKV}
    } \\
    &= \Delta \tau E^\mathrm{eff}_{m},
\end{align}
where terms $\mathcal{K}$ and $E^\mathrm{eff}$ can be interpreted as the kinetic energy and an effective energy of the time step.
%This notation differs slightly from reference~\cite[p.~11]{kylanpaaFirstprinciplesFiniteTemperature2011} due to the inclusion of normalization constant.
The probability amplitude~\ref{eq:probAmplitudeSampling} becomes
\begin{align}
    \mathcal{\NC} e^{- S'[\RR'(\tau)]}
        &=\lim_{M \to \infty} \prod_{m=0}^{M-1} \NC e^{- \Delta \tau E^\mathrm{eff}_{m}}\label{eq:subsEffectiveV}\\
    &= \mathcal{\NC} e^{- \beta E^\mathrm{eff}[\RR'(\tau)]}\label{eq:EffectiveVPath}\\
    &\propto P(\vec{x}_i)\nonumber,
\end{align}
The term $E^\mathrm{eff}[\RR'(\tau)]$ is real-valued, and it can be thought of as an effective energy of whole ''path polymer''. Because the amplitude $e^{-\beta E^\mathrm{eff}}$ is non-negative, it can be interpreted as the Boltzmann coefficient.~\cite{shumwayPathIntegralMonte2000}

Consider a transition of path that affects only one particle $n$ at time slice $m$. In the product~\ref{eq:subsEffectiveV}, most of the terms remain constant, as only the terms $E^\mathrm{eff}_{m-1}$ and $E^\mathrm{eff}_{m}$ depend on $\RR_m$. If~\ref{eq:subsEffectiveV} is substituted to acceptance probability~\ref{eq:uncutAcceptProb}, the constant terms cancel, giving
\begin{align*}
    q(\vec{x}_i \rightarrow \vec{x}_{i+1}) = \frac{T(\vec{x}_{i+1} \rightarrow \vec{x}_{i})
        e^{- \Delta \tau \pa{
            E^\mathrm{'eff}_{m-1}
            + E^\mathrm{'eff}_{m}
        }}
    }{T(\vec{x}_i \rightarrow \vec{x}_{i+1})
        e^{- \Delta \tau \pa{
            E^\mathrm{eff}_{m-1}
            + E^\mathrm{eff}_{m}
        }}
    },
\end{align*}
where $E^{'\mathrm{eff}}$ and $E^\mathrm{eff}$ notate energy in proposed position $\vec{x}_{i+1}$ and current position $\vec{x}_i$ respectively.
Similarly, equation~\ref{eq:EffWithKV} shows that the terms that do not contain $n$, remain constant in the transition, and so the acceptance probability~\ref{eq:uncutAcceptProb} becomes
\begin{align*}
    q(\vec{x}_i \rightarrow \vec{x}_{i+1}) &=
    \frac{
        T(\vec{x}_{i+1} \rightarrow \vec{x}_{i})e^{- \Delta \tau \pa{
            \mathcal{K}'_{n,m-1} + \mathcal{K}'_{n,m}
            + \sum_{n'} V'_{n,n',m}
        }}
    }{
        T(\vec{x}_i \rightarrow \vec{x}_{i+1}) e^{- \Delta \tau \pa{
            \mathcal{K}_{n,m-1} + \mathcal{K}_{n,m}
            + \sum_{n'} V_{n,n',m}
        }}
    }\\
    &= \frac{
        T(\vec{x}_{i+1} \rightarrow \vec{x}_{i})}{T(\vec{x}_i \rightarrow \vec{x}_{i+1})}
        e^{- \Delta \tau \pa{\Delta \mathcal{K} + \Delta V}}.
\end{align*}
This means that a full trajectory does not need to be evaluated for the calculation of $q$, but only the changed interactions need to be re-evaluated.

The computation time of $q$ can be reduced even further by cleverly choosing the transition function. Note that a free particle kernel~\ref{eq:freePrtKernel} is a Gaussian function in imaginary time. If $T$ is chosen as the 3-dimensional Gaussian distribution with variance $\frac{\hbar^2}{2m_n}\Delta\tau$, then~\cite{kylanpaaFirstprinciplesFiniteTemperature2011}
\begin{align}
    \frac{T(\vec{x}_{i+1} \rightarrow \vec{x}_{i})}{T(\vec{x}_i \rightarrow \vec{x}_{i+1})} e^{- \Delta \tau \Delta \mathcal{K}} = 1,
\end{align}
which leads to the transition probability
\begin{align}
    q(\vec{x}_i \rightarrow \vec{x}_{i+1}) &= e^{- \Delta \tau \Delta V}.
\end{align}
This means that the kinetic energy does not need to be sampled at all, and it is known as \textit{bisection method}.



\section{Symmetry considerations} \label{sec:symmetryFB}
Indistinguishable particles pose some challenges in the sampling process. The density matrix of indistinguishable fermions and bosons is~\cite{shumwayPathIntegralMonte2000}
\begin{align}
    \rho_{F/B}(\RR_a, \RR_b, \beta)
        = \frac{1}{N!} \sum_\mathcal{P} (\mp 1)^\mathcal{P}
            \int_{\RR'(0)=\RR_a}^{\RR'(\beta)=\mathcal{P}\RR_b} \mathcal{D}\RR'(\tau) \;
            \mathcal{\NC} e^{- S'[\RR'(\tau)]}. \label{eq:rhoFB}
\end{align}
where $\mathcal{P}$ denotes a permutation of indistinguishable particle labels, and signs $-$ and $+$ correspond to fermions and bosons respectively. Effectively, the summation of permutations connects trajectories of different particles forming longer shared trajectories. The sum over permutations can be sampled with Monte Carlo along with the sampling of paths.

In case of fermions, the minus sign in equation~\ref{eq:rhoFB} poses a problem in sampling. The probability amplitude of combined path may be negative, which can not be directly interpret as a probability. There are methods to circumvent this~\cites{kylanpaaFirstprinciplesFiniteTemperature2011}{shumwayPathIntegralMonte2000}, but they are computationally complex to calculate. The problem is known as the \textit{fermion sign problem}, which is proven to be NP-hard~\cite[p.~72]{tiihonenThermalEffectsAtomic2019}, meaning that there does not exist a computationally efficient method for it.

The symmetry of fermions and bosons is not considered in this work, as all of the inspected systems contain only distinguishable particles. For example, the particles forming the hydrogen molecule are distinguishable when both two protons and two electrons are assumed to have opposing spins. If the overlap of the identical particles is negligible, the indistinguishability need not be considered.




\section{PIMC path sample}
Even though illustrative example of PIMC path was given in figure~\ref{fig:schematicIllustrationPaths}, it does not reflect common simulations in practice, as the number of time slices is usually much higher. Figure~\ref{fig:H2paths} plots two actual samples from a hydrogen molecule simulation. The number of time slices is $21052$ and $2105$ for temperatures $\SI{300}{\kelvin}$ and $\SI{3000}{\kelvin}$ respectively. At lower temperature, the path of particle is longer, because higher inverse temperature $\beta$ corresponds to longer imaginary time. A thermal wave length of nucleus is clearly visible, and it increases at lower temperatures.
\begin{figure}[!tbh]
     \makebox[\textwidth][c]{
        \begin{subfigure}{.50\textwidth}
            \centering
            \includegraphics[width=1.0\textwidth]{figures/path_T300.png}
            \vspace*{-0.5cm}\caption{$T = \SI{300}{\kelvin}$}
            \label{fig:H2pathsT300}
        \end{subfigure}%
        %\hfill
        \begin{subfigure}{.50\textwidth}
            \centering
            \includegraphics[width=1.0\textwidth]{figures/path_T3000.png}
            \vspace*{-0.5cm}\caption{$T = \SI{3000}{\kelvin}$}
            \label{fig:H2pathsT3000}
        \end{subfigure}
        $\qquad$$\quad$
     }
    \caption{Random path samples of the hydrogen molecule $\mathrm{H}_2$ from an actual simulation. Green and blue lines correspond to electrons, and red and orange lines correspond to nuclei.}
    \label{fig:H2paths}
\end{figure}

% \section{Action}
% \section{Approximations and methods}
% \subsection{Primitive approximation}
% \subsection{Pair approximation}
% \subsection{Matrix squaring}
% \section{Calculation of properties}
% \subsection{Diagonal properties}
% \subsection{Thermal energy estimator}
% \subsection{Magnetic suscpetibility}
% \section{Symmetry of identical particles}
