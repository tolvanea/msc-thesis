\chapter{Dynamic susceptibility}
\label{ch:DynamicSusc}
The dynamic susceptibility is derived for a time-dependent system by using the operator formalism. The system is at zero temperature.


\section{Linear response theory}
Let $H_0$ be a time-independent Hamiltonian of a homogenic system in equilibrium. An external time-dependent perturbation $\hat{H}_\mathrm{ext}(t)$ is turned on at time $t=0$ so that $\hat{H}_\mathrm{ext}(t)=0$ if $t<0$. A total Hamiltonian is then
\begin{align}
    \hat{H}(t) = \hat{H}_0 + \hat{H}_\mathrm{ext}(t). \label{eq:pertHamiltonian}
\end{align}
The perturbation is formed by some external field $F(t)$ interacting to an internal variable, which has an observable $\hat{Q}$. The perturbation is
\begin{align}
    \hat{H}_\mathrm{ext}(t) = - F(t) \hat{Q}, \label{eq:extForce}
\end{align}
where $F(t) = 0$ if $t<0$. For example, $F(t)$ can be a strength of the electric field in $x$-direction and $\hat{Q}$ can be the electric polarization operator in the same direction. Alternatively, $F(t)$ and $\hat{Q}$ can correspond to the magnetic field and the magnetic moment operator respectively. This is similar to the section~\ref{ch:staticSusc}. Note that both $F(t)$ and $\ev{\hat{Q}}$ are real valued quantities.

At $t<0$ the system is in the equilibrium, i.e. $\ev{\hat{Q}}(t) = \ev{\hat{Q}}_{\hat{H}_0}$. The time dependence of $\ev{\hat{Q}}(t)$ can be expanded with series~\cite{andreitokmakoff74IntroductoryQuantum2009}
\begin{align}
    \ev{\hat{Q}}(t) &= \pa{\text{terms }F^{(0)}} + \pa{\text{terms }F^{(1)}} + \cdots\\
    &= \ev{\hat{Q}}_{\hat{H}_0} + \delta \ev{\hat{Q}}(t) + \cdots
\end{align}
where $\delta \ev{\hat{Q}}(t)$ denotes a linear response term. If the perturbation is small, the linear response is enough to describe the system. It is
\begin{align}
    \delta \ev{\hat{Q}}(t) = \int_{0}^{\infty} \dd t' \chi(t') F(t-t'), \label{eq:suscResponse}
\end{align}
where $\dd t'$ integrates all times backwards in history, and $\chi(t')$ is a linear response function, which determines how much past values of $F(t-t')$ affect the current value of $\ev{\hat{Q}}(t)$. For example, if the magnetic field is suddenly applied to the material, the material takes some time to fully magnetize, and the response function determines that memory effect. It can be easily shown that the linear response function $\chi(t')$ is an impulse response, that is, if $F(t-t')$ is replaced with the Dirac delta function $\delta(t-t')$, then
\begin{align}
    \int_{0}^{\infty} \dd t' \chi(t') \delta(t-t') = \chi(t),
\end{align}
meaning that $\delta \ev{\hat{Q}}(t) = \chi(t)$ for the impulse $F(t) = \delta(t)$. It is also evident that $\chi(t)$ is a real function.

By defining that $\chi(t') = 0$ when $t'<0$, the equation~\ref{eq:suscResponse} can be written without the lower integration bound
\begin{align}
     \delta \ev{\hat{Q}}(t) = \int_{-\infty}^{\infty} \dd t' \chi(t') F(t-t'). \label{eq:linearResponceClassic}
\end{align}
This definition of $\chi$ is useful, because the equation~\ref{eq:linearResponceClassic} can be expressed with a \textit{convolution} of $\chi$ and $F$. The convolution of two functions $f(t)$ and $g(t)$ is defined as
\begin{align}
    \pa{f * g}(t) \equiv \int_{-\infty}^{\infty} \dd t' f(t') g(t-t').
\end{align}
A fourier transform of the convolution has a simple expression
\begin{align}
    \pa{\tilde{f * g}}(\omega) = \tilde{f}(\omega) \tilde{g}(\omega),
\end{align}
where the Fourier transform is defined as
\begin{align}
    \tilde{f}(\omega) = \int_{-\infty}^{\infty} \dd t f(t) e^{- \ii \omega t}\\
    \tilde{f}(t) = \frac{1}{2\pi}\int_{-\infty}^{\infty} \dd \omega \tilde{f}(\omega) e^{\ii \omega t}.
\end{align}
Therefore, the equation~\ref{eq:linearResponceClassic} can be written as the convolution in frequency domain with
\begin{align}
    \ev{\hat{\tilde{Q}}}(\omega) = \tilde{\chi}(\omega) \tilde{F}(\omega).
\end{align}
The response function $\chi$ is also called the \textit{dynamic susceptibility}.


\section{Dynamic susceptibility}
This section derives a following formula for the dynamic susceptibility
\begin{align}
    \chi(t) = \frac{\ii}{\hbar} \theta(t) \ev{\sq{\hat{Q}(t), \hat{Q}(0)}}, \label{eq:dynsusc}
\end{align}
where $\sq{\hat{A},\hat{B}} = \hat{A}\hat{B}-\hat{B}\hat{A}$ is the commutator, and the expectation value $\ev{\cdot}$ corresponds to the unperturbed system. The derivation follows loosely sources~\cite[p.~1262]{kleinertPathIntegralsQuantum1990}\cite[p.~8-11]{andreitokmakoff74IntroductoryQuantum2009}

The perturbed Hamiltonian $\hat{H} = \hat{H}_0 + \hat{H}_\mathrm{ext}$ from equation~\ref{eq:pertHamiltonian} has the time evolution operator
\begin{align}
    \hat{U}(t) &= e^{-\frac{\ii}{\hbar} \int_0^t \hat{H}(t)}\\
    &= e^{-\frac{\ii}{\hbar} \pa{\hat{H}_0 t + \int_0^t \dd t' \hat{H}_\mathrm{ext}(t')}}\\
    &= e^{-\frac{\ii}{\hbar} \hat{H}_0 t} \hat{U}_\mathrm{ext}(t).
\end{align}
The expectation value of a Schrödinger observable $\hat{P}$ in the perturbed state is
\begin{align}
 \mel{\phi(t)}{\hat{P}}{\phi(t)} &= \mel{\hat{U}(t) \phi(0)}{\hat{P}}{\hat{U}(t) \phi(0)}\\
 &= \mel{\phi(0)}{\hat{U}^*(t) \hat{P} \hat{U}(t)}{\phi(0)}\\
    &= \mel{\phi(0)}{
        \hat{U}_\mathrm{ext}^*(t)
        \underbrace{e^{+\frac{\ii}{\hbar} \int_0^t \dd t' \hat{H}_0(t')} \;\hat{P}\; e^{-\frac{\ii}{\hbar} \int_0^t \dd t' \hat{H}_0(t')}}_{\hat{P}_{\hat{H}_0}(t)}
        \hat{U}_\mathrm{ext}(t)
    }{\phi(0)}, \label{eq:heisenberg}
\end{align}
where the operator $\hat{P}_{\hat{H}_0}(t)$ measures the unperturbed system in the Heisenberg picture. In the Heisenberg picture, the time dependence has been moved from the state $\phi$ to the operator $\hat{P}$.

%Terms in $\hat{U}_\mathrm{ext}(t)$ are described by terms in Zassenhaus formula~\cite{casasEfficientComputationZassenhaus2012}
%\begin{align}
%    e^{\hat{X}+\hat{Y}} =
%    e^{\hat{X}} e^{\hat{Y}}
%    e^{\frac{1}{2} \sq{\hat{X},\hat{Y}}}
%    e^{\frac{1}{6}\pa{\sq{\hat{X},\sq{\hat{X}, \hat{Y}}} + 2\sq{\hat{Y},\sq{\hat{X}, \hat{Y}}}}} \cdots
%\end{align}
The time evolution of the perturbation can be expressed with the series~\cite[p.~1263]{kleinertPathIntegralsQuantum1990}
\begin{align}
    \hat{U}_\mathrm{ext}(t) = 1 - \frac{\ii}{\hbar} \int_0^t \dd t' \hat{H}_\mathrm{ext}(t') + \cdots,
\end{align}
with which the equation~\ref{eq:heisenberg} becomes
\begin{align}
    &\mel{\phi(0)}{
        \pa{1 + \frac{\ii}{\hbar} \int_0^t \dd t' \hat{H}_\mathrm{ext}(t') + \cdots}
        \hat{P}_{\hat{H}_0}(t)
        \pa{1 - \frac{\ii}{\hbar} \int_0^t \dd t' \hat{H}_\mathrm{ext}(t') + \cdots}
    }{\phi(0)}\\
    = &\mel{\phi(0)}{ \hat{P}_{\hat{H}_0}(t) }{\phi(0)} \;-\;
    \frac{\ii}{\hbar} \int_0^t \dd t'
    \mel{\phi(0)}{\sq{\hat{P}_{\hat{H}_0}(t), \hat{H}_\mathrm{ext}(t')}}{\phi(0)}
    \;+\; \cdots. \label{eq:heisenberg2}
\end{align}
This can be written in a more compact form
\begin{align}
    \ev{\hat{P}}(t) &\approx \ev{\hat{P}}_{\hat{H}_0}
    \; \underbrace{-\; \frac{\ii}{\hbar} \int_0^t \dd t' \ev{\sq{\hat{P}_{\hat{H}_0}(t), \hat{H}_\mathrm{ext}(t')}}_{\hat{H}_0}}_{\delta \ev{\hat{P}}(t)}
    \;+\; \cdots,
\end{align}
where the linear term is
\begin{align}
    \delta \ev{\hat{P}}(t) = - \frac{\ii}{\hbar} \int_0^t \dd t' \ev{\sq{\hat{P}_{\hat{H}_0}(t), \hat{H}_\mathrm{ext}(t')}}_{\hat{H}_0}. \label{eq:kubo}
\end{align}
If the perturbation is small, higher order terms vanish, and only the linear term is left. It is worth noting that the averages correspond to the unperturbed system, so the small perturbation can be calculated without the simulation of the perturbed system.

By inserting $\hat{H}_\mathrm{ext}(t) = -F(t) \hat{Q}_{\hat{H}_0}(t)$ from equation~\ref{eq:extForce} to equation~\ref{eq:kubo} gives
\begin{align}
    \delta \ev{\hat{P}}(t)
    &= - \frac{\ii}{\hbar} \int_0^t \dd t' \ev{\sq{\hat{P}_{\hat{H}_0}(t), -F(t') \hat{Q}_{\hat{H}_0}(t')}}_{\hat{H}_0}\\
    &= \frac{\ii}{\hbar} \int_0^t \dd t' \ev{\sq{\hat{P}_{\hat{H}_0}(t), \hat{Q}_{\hat{H}_0}(t')}}_{\hat{H}_0} F(t')\\
    &= \frac{\ii}{\hbar} \int_{0}^{t} \dd t'' \ev{\sq{\hat{P}_{\hat{H}_0}(t), \hat{Q}_{\hat{H}_0}(t - t'')}}_{\hat{H}_0} F(t - t''),\label{eq:uglyTrickIsThisEvenValid}
\end{align}
where the last line applies a change of variables $t' = t - t''$. The last line also flips the integration limits reversing the integration order, which may not be apparent at the first glance. A reference point of the Heisenberg operators can be shifted from $t=0$ to $t=-\infty$, which changes the upper integration limit of~\ref{eq:uglyTrickIsThisEvenValid} from $t$ to $\infty$. Operators under the expectation value can be time-shifted by $-(t - t'')$, because the corresponding Schrödinger operators are time-independent. These manipulations result the integral in form
\begin{align}
    \delta \ev{\hat{P}}(t) = \frac{\ii}{\hbar} \int_0^{\infty} \dd t'' \ev{\sq{\hat{P}_{\hat{H}_0}(t''), \hat{Q}_{\hat{H}_0}(0)}}_{\hat{H}_0} F(t - t'').
\end{align}
%This time shifting property can be easily verified by expanding $\hat{O}_{\hat{H}_0}(t) = e^{\frac{\ii}{\hbar} H_0 t} \hat{O} e^{-\frac{\ii}{\hbar} H_0 t}$ in energy eigenstate basis, in which time evolution is $e^{-\frac{\ii}{\hbar} H_0 t} \ket{n} = e^{-\frac{\ii}{\hbar} E_n t} \ket{n}$.
Last, the lower integration bound can be removed by inserting a Heaviside step function $\theta(t)$, which results
\begin{align}
    \delta \ev{\hat{P}}(t) = \frac{\ii}{\hbar} \int_{-\infty}^{\infty} \dd t'' \theta(t'') \ev{\sq{\hat{P}_{\hat{H}_0}(t''), \hat{Q}_{\hat{H}_0}(0)}}_{\hat{H}_0} F(t - t''). \label{eq:moreGeneralDynSusc}
\end{align}
If this is compared to equation~\ref{eq:linearResponceClassic}, the dynamic susceptibility is recognized as
\begin{align}
    \chi_{\hat{P}\hat{Q}}(t) = \frac{\ii}{\hbar} \theta(t) \ev{\sq{\hat{P}_{\hat{H}_0}(t), \hat{Q}_{\hat{H}_0}(0)}}_{\hat{H}_0}. \label{eq:genDynSusc}
\end{align}
This equation is in more general form than equation~\ref{eq:dynsusc}, which has assigned $\hat{Q}$ in place of the operator $\hat{P}$. That is, $\chi(t) = \chi_{\hat{Q}\hat{Q}}(t)$. For example, if $F$, $\hat{Q}$, and $\hat{P}$ correspond to $B_x$, $\hat{m}_x$ and $\hat{m}_y$ respectively, then $\chi_{\hat{m}_y\hat{m}_x}(t)$ describes how the magnetic moment $\hat{m}$ is induced in $y$-direction by having the magnetic field $B$ in $x$-direction. This equation can be also utilized in calculation of higher order responses, such as a quadrupole response~\cite{tiihonenThermalEffectsAtomic2019}.
%However, since this thesis inspects only magnetic susceptibility in isotropic materials, the simpler form~\ref{eq:dynsusc} is used.% ($\hat{P} = \hat{m}_x$) ($\hat{Q}=m_y$, $F=H_y$)









\chapter{Diamagnetism of atomic hydrogen}
\label{app:diamagnHydrogen}
An alternative representation of the static magnetic susceptibility is derived with the operator formalism. The exact diamagnetic susceptibility is calculated for the Born--Oppenheimer hydrogen atom at zero temperature.

Consider one electron system that has the nucleus fixed in origin. Shall the system be affected by the external magnetic field. The magnetic interaction is included into the Hamiltonian with a minimal substitution $\hat{\vec{p}} \to \hat{\vec{p}} - q \vec{A}(\vec{r})$, which is
\begin{align}
    \hat{H} = \frac{\pa{\hat{\vec{p}} - q \vec{A}(\vec{r})}^2}{2m} + V(\vec{r}). \label{eq:magnHamiltonian}
\end{align}
Applying a homogeneous magnetic field $\vec{B} = \sq{0,0,B}^T$ to equation~\ref{eq:vecPotCoulombGauge} gives the vector potential
\begin{align}
    \vec{A}(x,y,z) = \frac{B}{2} \sq{-y, x, 0}^T.
\end{align}
By utilizing properties of the Coulomb gauge, the Hamiltonian can be written~\cite[p.~180]{kleinertPathIntegralsQuantum1990}\cite[p.~11]{noltingQuantumTheoryMagnetism2009}\cite[p.~162]{hofmannSolidStatePhysics2015}
\begin{align}
    \hat{H} &= \pa{\frac{\hat{\vec{p}}^2}{2m} + V} - \frac{q}{2 m} \pa{\vec{r} \cross \hat{\vec{p}}}_z B + \frac{q^2 B^2}{8m} \pa{x^2 + y^2}\\
    &= \hat{H}_0 - \gamma \hat{\vec{L}}_z B + \frac{q^2 B^2}{8m} \pa{x^2 + y^2}, \label{eq:magnHamiltonianOpen}
\end{align}
where $\hat{H}_0$ is the Hamiltonian without magnetic field and $\hat{\vec{L}} = \vec{r} \cross \hat{\vec{p}}$ is the angular momentum operator. The reference point of $\pa{x^2 + y^2}$ is at the origin, where the hydrogen nucleus also lies. The Hamiltonian can be also written in form
\begin{align}
    \hat{H} = \hat{H}_0 - \hat{m}_z B,
\end{align}
where the magnetic moment operator in $z$-direction is %
\begin{align}
    \hat{m}_z &= -\pdv{\hat{H}}{B}\\
    &= \gamma \hat{\vec{L}}_z - \frac{q^2}{4m} \pa{x^2 + y^2} B. \label{eq:magneticMomentV2}
\end{align}
The first term in~\ref{eq:magneticMomentV2} is the permanent magnetic moment, which does not vanish at $B=0$. The second term is the induced magnetic moment, which corresponds to diamagnetism.


At zero temperature, the system is in the ground state, and the electron has no orbital angular momentum. Also, the diamagnetism does not consider the spin angular momentum, and so the total angular momentum is considered to be zero. The equation~\ref{eq:magneticMomentV2} becomes
\begin{align}
    \hat{m}_z = - \frac{e^2}{4m_e} \pa{x^2 + y^2} B,
\end{align}
which has substituted $q=-e$. Equations~\ref{eq:suscAsADerivativev2} and~\ref{eq:suscExtraTerm} give then the magnetic susceptibility
\begin{align}
    \chi_z^\mathrm{mag} &= \mu_0 \lim_{B \to 0}\pa{\pdv{\ev{\hat{m}}}{B}}\\
    &= -\mu_0 \frac{e^2}{4m_e} \ev{x^2 + y^2}. \label{eq:hydrogenSuscExpanded}
\end{align}
Because the ground state is spherically symmetric,
\begin{align}
    \ev{x^2 + y^2} = \frac{2}{3} \ev{r^2}.\label{eq:twoThirds}
\end{align}
The ground state has a radial distribution function~\cite[p.~88]{atkinsMolecularQuantumMechanics2011}
\begin{align}
    P(r) = 4a^{-3}r^2 e^{-\frac{2r}{a}},
\end{align}
where
\begin{align}
    a = \frac{4 \pi \varepsilon_0 \hbar^2}{\mu e^2}.
\end{align}
Here $\mu$ is either the mass of the electron $m_e$ or the reduced mass $\frac{m_p m_e}{m_p + m_e}$, depending on whether the Born Oppenheimer approximation is used or not. The expectation value of $\ev{r^2}$ can be integrated analytically to
\begin{align}
    \ev{r^2} = \int_0^\infty P(r) r^2 \dd r = 3a^2. \label{eq:r2integral}
\end{align}
Because the Born Oppenheimer approximation was assumed earlier, $\mu=m_e$, and $a$ is Bohr radius $a_0$.
Inserting the equations~\ref{eq:r2integral} and~\ref{eq:twoThirds} to~\ref{eq:hydrogenSuscExpanded} yields an analytical form
\begin{align}
    \chi^\mathrm{mag} &= -\frac{\mu_0 e^2}{4m_e} 2 a_0^2\\
    &= -\frac{\mu_0 e^2}{2m_e} a_0^2.
\end{align}
The numerical value per mole is
\begin{align}
    \chi_\mathrm{mol}^\mathrm{mag} &= \chi^\mathrm{mag} N_A\\ &\approx \SI{-2.9858e-11}{\frac{\meter^3}{\mol}},
\end{align}
where $N_A$ is the Avogardo constant $\approx 6.022 \times 10^{23}\;\frac{1}{\si{\mol}}$.


\chapter{Supplementary information}
\label{ch:sorcery}
The simulation results and the simulation parameters are accessible in more details via an online repository~\cite{tolvanenSimulationDataThis}. This repository contains
\begin{itemize}
    \item more observables from the simulations, such as
        \begin{itemize}
            \item[--] non-extrapolated data points with the finite $\Delta \tau$,
            \item[--] pair distances of the particles $\ev{\talller{}r_{i,j}}$, $\ev{r_{i,j}^2}$, and $\ev{r_{i,j}^{-1}}$,
            \item[--] pair correlation distributions $P(r_{i,j})$
            \item[--] the susceptibilities for each particle individually, that is, terms proportional to the square area of the single particle $\ev{\ScalA^2}$,
            \item[--] cross terms between the coordinate component of the area $\ev{\ScalA_x \ScalA_y}$, which is statistically zero for all the systems, and
            \item[--] Monte Carlo walker data from the equilibrium convergence,
        \end{itemize}
    \item more figures from the simulations,
    \item parameters and templates that can be used to replicate all the results,
    \item scripts that apply unit conversions to calculate the reference values,
    \item automation and data analysis code written in Python, totaling over 7000 lines of code, and
    \item {\LaTeX} source code of this thesis.
\end{itemize}
The simulations are made with Fortran-based \texttt{pimc3} program, developed in our research group. The program is not yet published, so please contact us~\cite{tiihonenThermalEffectsAtomic2019, ElectronicStructureTheory} for the access. We are grateful for IT Center of Science Ltd. (CSC) for providing computation resources via their high-performance clusters.

