# Brief documentation of content
This directory contains:

* templates that can be used to replicate simulations
* data and figures from simulations

## Result replication

Directories named `sample` are templates that can be used to generate same calculations as presented in thesis. See my git repo https://gitlab.com/tolvanea/PIMC-automatization for more detailed information of use the scripts.

Result replication requires some work and it is probably not a few-hour job. I bet it can take few days to set everything up, plus simulation time from a day to a week. Main obstacles are:
    * asking the source code of pimc3 program from us (sorry for the inconvenience)
    * assuring dependencied for pimc3 program, and compiling it on cluster computer
        * If the cluster is not _CSC Puhti_, you need to write your own SBATCH.sh file
        * Installation on a home pc is a bit easier than on cluster, but it may require two weeks of computation time to get comparable accuracy for a single system simulated with multipletimesteps and temperatures.
    * downloading my python script tools (PIMC_automatization) and setting them to be easily accessible via `$PYTHONPATH`


### Running simulations

Assuming that everything is set up as described above. In nutshell, to run simulations you need to
* download my automatization code and add it to PYTHONPATH
* install pimc3 and set path to `configure_instance.py`
* start calculation with command `python3 -m start_PIMC.run_set --start`

### Analyzing results and plotting figures
Numerical results can be obtained by running command `python3 -m plot_data.main --susc` in . All figures of thesis are generated with `python3 -m plot_data.main --susc --calc_comb`, which is ugly hack to combine multible calculations into one figure.

## Understanding output files

### Text files
* `print_data.txt` - Energy tables
* `magnetic_susceptibility.txt` - Susceptibilities, which are marked with symbol "`<A^2>`". The same file may also contain
    * partial susceptiblities for positive/negative particles "`e_only`"/"`p_only`"
    * partial susceptiblities for each particle individually, eg. "`prt1_e`" for particle 1 that is electron.
    * nonisotropic susceptibility cross term `<Ax * Ay>`, marked a bit counter intuitively with "`cross_terms_<r_i*r_j>`"
    * susceptiblities calculated with Rebane's <r^2> formula "`<r^2>`"
* "print_data_distances.txt" - Particle pair separation distances / pair correlation

Oh, and if you wonder what "flatpad" means in text files, it is my pc name, which has been printed as loggin purposes.

### PDF files
* `energy_Vt.pdf` - energy plot
* `susceptibility.pdf` - susceptibility plot for this simulation
* `susceptibility_combination.pdf` - susceptibility plot that that (may) have combined data also from other simulations in the same parent directory. All curves are combined in that simulation that has directory name last by name sort. That is, if calculations are "He_AQ" and "He_BO", true combined plot can be found from "He_BO". This combination is ugly hack yes, but it plots the images I need.
* `susceptibility_e_only.pdf` - Partial susceptility that has included only negative particles. Positive particles has similar plot.
* `susceptibility_prt1_e.pdf` - Partial susceptility that has included only particle 1 which is electron "e". Other particles have similar plots.
* `susceptibility_cross_terms_<r_i*r_j>.pdf` - Nonisotropic susceptibilities
* `paircorrelation_distributions.pdf` Plots pair correlation distribution between particles.
* `OBSERABLE_NAME_T=300.00-3000.00K.pdf` - Plot convergence of simulation block by block. OBSERABLE_NAME can be "E/Et-Vt" for energy, "MAGN∕dia.susc._(qA)^2_mean" for susceptibility or "e_p∕Rave_" for particle separation distance. This graph shows where automatic convergence detection has placed converged starting block. Solid line means that blocks are considered converged. Data is swept with multiple averaging windows which are also plotted to smooth out noise. This figure may be packed with too much information to be readable, because it is meant to be zoomed and scaled within matplotlib's figure window.

(All information presented in this readme may not be present in files.)
