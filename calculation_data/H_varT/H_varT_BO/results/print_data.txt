In directory /home/alpi/Documents/Koulu/kurssit/nykyiset/dippa/msc-thesis/calculation_data/H_varT/H_varT_BO
flatpad 2021-04-07T17:59:41



Simulation blocks:
    \tau  T|T300.0       |T1000.0      |T3000.0      |
    --------------------------------------------------
    tau0.01|s8374/66834   s8556/68411   s766/6033     
    tau0.03|s2730/21706   s1883/14926   s1212/9536    
    tau0.05|s8000/30210   s1528/12147   s917/7323     
Notation above:
    s<S>/<N>': all success
where N is total number of blocks and S is convergence point.
All observable averages are calculated from blocks S-N.


Energy /E/Et
    \tau  T|T300.0      |T1000.0     |T3000.0     |
    -----------------------------------------------
    tau0.0 |-0.4993(7)  |-0.5001(8)  |-0.5005(20) |
    tau0.01|-0.4995(7)  |-0.4998(7)  |-0.5007(17) |
    tau0.03|-0.4999(4)  |-0.5005(6)  |-0.4997(8)  |
    tau0.05|-0.50013(35)|-0.4998(5)  |-0.5004(8)  |


Energy /E/Vt
    \tau  T|T300.0      |T1000.0     |T3000.0     |
    -----------------------------------------------
    tau0.0 |-0.49997(8) |-0.49995(9) |-0.50007(17)|
    tau0.01|-0.49994(7) |-0.49994(6) |-0.50008(14)|
    tau0.03|-0.50000(8) |-0.49996(10)|-0.49996(12)|
    tau0.05|-0.49992(8) |-0.49994(11)|-0.50003(15)|

