import numpy as np
import subprocess
import os
import datetime

import miscellaneous.conf_modification as conf_modification
import miscellaneous.change_priority as change_priority
from start_PIMC.generate_dir_names import generate_directory_names
import read_data.utilities.read_particles as read_particles

help_text = """
This is portable file that is used to create full pimc3-calculation set of
variating parameter value. It s used to initialize the set of calculation for
'run_set'.

Usage:
    (0. If this file is 'configure_instance_SAMPLE.py', rename it to
        'configure_instance.py')

    1. modify 'configure_instance.py' to variate some initial value of simulation
        - Most important parameters are in 'InitialSetup'-class, change them.
        - When changing what parameters are varied, modify
            - self.latex_symbol1 &self.latex_symbol2
            - self.latex_unit1 & self.latex_unit2
            - self.range1 & self.range2
            - variate_value()-function

    2. make sure directory tree looks like following, by moving this file:
        sample
            configure_instance.py     <-- this current file
            PARTICLES
            CONF
            SBATCH.sh                 (required even though ran locally)

    3. Run in parent directory that contains directory `sample`
       $ python3 -m start_PIMC.run_set --start


"""


class InitialSetup():

    def __init__(self):
        # first variating value
        self.latex_symbol1 = "\\tau"  # Symbol is used in plots, e.g. "\\tau"
        self.latex_unit1 = ""       # e.g. "nm"
        self.range1 = [0.01, 0.03, 0.05]

        # second variating value
        self.latex_symbol2 = "T"
        self.latex_unit2 = "K"
        self.range2 = [300.0, 1000.0, 3000.0]

        # The following fields are mostly for sbatch jobs
        self.exe_args = "minimal"   # applies only for sbatch jobs
        # num_proc modifies SBATCH.sh (`num_proc==None` doesn't modify)
        self.num_proc = 1
        # this timelimit is replaced in CONF and SBATCH.sh
        # If either latex_symbol is "\\tau" or "T", then timelimit is scaled
        # using idx1=idx2=0 as a base value
        self.timelimit = 4

        self.latex_symbol3 = ""
        self.latex_unit3 = ""
        self.range3_val = "BO"
        self.unite_A2_figure_with_r2 = False

        self.logarithmic_y_axis = False
        self.logarithmic_x_axis = False
        self.extrapolate_range1 = True

        self.reference_values = {
            "$\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$"
                : [
                    ("ref. AQ", 0.0, -2.99071e-11),
                    ("ref. BO", 0.0, -2.98583e-11),
                    ("ref. BO", 0.0, -2.98577e-11, 1),
                    ("ylims", 2.997e-11, 2.9730e-11),
                    ],
            "$E$ ($E_h$)"
                : [("ref. AQ", 0.0, -0.49973),
                   ("ref. BO", 0.0, -0.5)]
        }

        # Test if this is run on taito-cluster or just locally .
        # Puhti-cluster
        if os.path.isdir("/users/tolvane2"):
            self.sbatch = True
            # name of executable, full path (or executable must be found from $PATH)
            self.executable = "/users/tolvane2/koodit/pimc3/build/pimc3"
        else:
            self.sbatch = False
            # Current computer is my laptop
            if os.path.isdir("/home/alpi/Vain_loota/"):
                self.executable = "/home/alpi/Vain_loota/ohjelmat/pimc3/build/pimc3"
            elif os.path.isdir("/home/alpi/Vain_flatpad/"):
                self.executable = "/home/alpi/Vain_flatpad/ohjelmat/pimc3/build/pimc3"
            else:
                self.executable = "NO BINARY"
                raise Exception("Configure pimc-binary yourself")

        # ------------------------------------------------------------
        # Other declarations, that are not needed to modify in any way.
        # ------------------------------------------------------------

        self.parent_dir_name = os.path.basename(os.path.dirname(os.path.dirname(__file__)))

        # String formatting (=constant width) of variating values. Without and
        # with base. (e.g. If range1=[1.00, ...] str_range1 = ["1.00",...] and
        # dir1_labels = ["Tau_1.00",...])
        self.str_range1, self.str_range2, self.dir1_labels, self.dir2_labels \
            = generate_directory_names(self)

        # This will be overwritten to True, if flag --no-run is passed.
        self.no_run = False

        # Overwrite_list list is used, when you want to continue simulation, but
        # update some files. For example if you decide to change temperature for
        # all calculations, call with "--overwrite CONF" flag. If you just want to
        # continue without changing any parameters, run with "--continue".
        self.overwrite_list = "new_run"  # will be modified later in overwrite mode

    """
    The following function is entry point from `start_PIMC.run_set`.
    """
    def set_up_simulation(self,
                          idx1: int,
                          idx2: int,
                          continuerun=False) -> None:
        num1 = self.range1[idx1]
        num2 = self.range2[idx2]
        """
        By running
            python3 -m start_PIMC.run_set --start
        sample-directory is copied for every simulation, and this function is called to do all the
        modifications to vary parameters.

        This function is called also with commands
            python3 -m start_PIMC.run_set --continue
            python3 -m start_PIMC.run_set --overwrite
        but then `continuerun` is True. With --overwrite flag, `self.overwrite_list` contains
        overwritable file names.

        This function is called per every differing configuration of calulation. So it is
        called from `start_PIMC.run_set` something like so:

        for num1 in InitialSetup.range1
            for num2 in InitialSetup.range2
                set_up_simulation(num1, num2)
        """

        # Make sure at least temperature and tau are not changed without
        # explisit modifications in 'overwrite_list'
        self.check_overwrite_list(num1, num2, continuerun)

        # variate simulation setup somehow for each value
        # (write your own custom function for that)
        self.variate_value(num1, num2)

        # Modify sbatch file on puhti
        modify_SBATCH_file(self, idx1, idx2, continuerun)

        # calls correct executable (desktop or sbatch job)
        self.run_pimc3()

    def variate_value(self, num1, num2) -> None:
        """
        This function does modifications to copy of simulation template. Two variating values
        are changed.

        :param num1:    variating value on first directory-level
        :param num2:    variating value on second directory-level
        """
        if (self.overwrite_list == "new_run") or ("CONF" in self.overwrite_list):
            modify_Tau(num1, self.latex_symbol1)
            modify_T(num2, self.latex_symbol2)

        print("Running: {}={},  {}={}".format(self.latex_symbol1, num1,
                                              self.latex_symbol2, num2))

    def check_overwrite_list(self, num1, num2, continuerun):
        """ Make some checks whether some most common parameters are changed in
        the middle of calculation, and throw error if required filed are not in
        overwrite_list.

        Now, when I think, this is quite useless function, if not expanded.
        Why would anyone change T or Tau in mid calculation, if dimensions can
        not match?
        TODO add more checks, or actually, compare new and old file in run_set.py
        """

        if not continuerun:
            if self.overwrite_list != "new_run":
                raise Exception('New calculations must have: self.overwrite_list = "new_run"')#
        else:
            if self.overwrite_list == "new_run":
                raise Exception("Please state explisitly that you are not modifying any parameter on this\n"
                                + "continued run. So use flag --continue or --overwrite instad of --start.")

            for num, latex_symbol in ((num1, self.latex_symbol1), (num2, self.latex_symbol2)):
                # check if value is temperature
                allowed_T = ["T"]
                if (latex_symbol in allowed_T):
                    T = float(conf_modification.read_line("CONF", key="Temperature")[0])
                    if (np.abs(num-T) > 0.001) and "CONF" not in self.overwrite_list:
                        raise Exception("You modified temperature, add 'CONF' in 'overwrite_list'")

                # check if value is time step
                allowed_tau = ["Tau", "tau", "\\tau"]
                if (latex_symbol in allowed_tau):
                    T = float(conf_modification.read_line("CONF", key="Tau")[0])
                    if (np.abs(num-T) > 0.001) and "CONF" not in self.overwrite_list:
                        raise Exception("You modified Tau, add 'CONF' in 'overwrite_list'")


    def run_pimc3(self):

        if self.no_run:
            subprocess.Popen(["echo", "not running", self.executable, "or anything"],
                             cwd=os.getcwd())
            return

        if self.sbatch:
            subprocess.Popen(["sbatch", "SBATCH.sh"], cwd=os.getcwd())

        else:
            # command = "stdbuf -o L " + self.executable + " " + self.exe_args + " 2>&1 | tee stdout.txt"
            # command = "stdbuf -e L -o L {} {} |& tee mylog".format(self.executable, self.exe_args)
            command = self.executable  # + " " + self.exe_args

            print("Running terminal, command: " + command)
            subprocess.Popen(["konsole", "--noclose", "--new-tab", "--workdir",
                              os.getcwd(), "-e",
                              command],  # "echo Hai! ", command, " && ",
                             cwd=os.getcwd())

            change_priority.main(["asdf", self.executable, "low"])


if __name__ == "__main__":
    raise Exception(
        "No, don't run this. Run 'run_set.py', which uses this as a configuration.")


def modify_Tau(num, latex_symbol):
    allowed = ["Tau", "tau", "\\tau"]
    if not (latex_symbol in allowed):
        raise Exception("You are modifying incorrect value. '{}' is not in {}."
                        .format(latex_symbol, allowed))

    num = np.around(num, decimals=5)  # prevent numbers like 1.0000000000003 in CONF

    conf_modification.old_bash_style_config_modifier(
        ["asdfg-unused", "-f", "CONF", "--Tau=" + str(num)])

def modify_T(num, latex_symbol):
    allowed = ["T"]
    if not (latex_symbol in allowed):
        raise Exception("You are modifying incorrect value. '{}' is not in {}."
                        .format(latex_symbol, allowed))

    num = np.around(num, decimals=5)
    # change temperature
    conf_modification.old_bash_style_config_modifier(
        ["asdfg-unused", "-f", "CONF", "--Temperature=" + str(num)])

def modify_SBATCH_file(selfobj, idx1, idx2, continuerun):
    """
    Modifies sbatch file for taito cluster.
    Note! This must be called after all modifications on CONF file.
    """
    sbatch = "SBATCH.sh"

    if (selfobj.overwrite_list != "new_run") and not (sbatch in selfobj.overwrite_list):
        return
    num_form1 = selfobj.str_range1[idx1]
    num_form2 = selfobj.str_range2[idx2]
    conf_modification.replace_line(file_name=sbatch,
                                   line="#SBATCH --job-name=",
                                   replacement="#SBATCH --job-name={}_{}_{}".format(
                                       selfobj.parent_dir_name, num_form1,
                                       num_form2))
    # set time limit
    timelim = selfobj.timelimit
    for sym, rang, idx in [(selfobj.latex_symbol1, selfobj.range1, idx1),
                           (selfobj.latex_symbol2, selfobj.range2, idx2)]:
        if sym in ["\\tau", "T"]:  # Inverse scaling to complexity of calculations
            timelim *=  rang[0]/rang[idx]
    timelim = int(timelim+0.5)

    if timelim <= 72:
        conf_modification.replace_line(file_name=sbatch,
                                    line="#SBATCH --time=",
                                    replacement="#SBATCH --time={:02}:00:00"
                                    .format(timelim))
    else:
        # BTW, you need longrun partition for this one
        conf_modification.replace_line(file_name=sbatch,
                                    line="#SBATCH --partition=",
                                    replacement="#SBATCH --partition=longrun")
        conf_modification.replace_line(file_name=sbatch,
                                    line="#SBATCH --time=",
                                    replacement="#SBATCH --time={:1}-{:02}:00:00"
                                    .format(timelim // 24,
                                            timelim % 24))
    conf_modification.old_bash_style_config_modifier(
        ["asdfg-unused", "-f", "CONF",
        "--TimeLimit={}".format(timelim)])

    time = datetime.datetime.now().replace(microsecond=0).isoformat()[:-3]

    # Set output file names to contain date (this prevents overwrite if calculation is continued)
    conf_modification.replace_line(file_name=sbatch, line="#SBATCH -o",
                                   replacement="#SBATCH -o out_{}.txt".format(time))
    conf_modification.replace_line(file_name=sbatch, line="#SBATCH -e",
                                   replacement="#SBATCH -e err_{}.txt".format(time))

    # Set executable
    conf_modification.replace_line(file_name=sbatch,
                                   line="srun",
                                   replacement="srun {} {}".format(
                                       selfobj.executable, selfobj.exe_args))

    # This sets number of used processors i.e. MPI processes i.e. slurm tasks.
    # Note: pimc3 achieves parallelism with MPI processess, not with threads.
    # (Well ok, there is thread support with OpenMP, but MPI processes work a bit better.)
    if selfobj.num_proc is not None:
        conf_modification.replace_line(file_name="SBATCH.sh",
                                       line="#SBATCH --ntasks=",
                                       replacement="#SBATCH --ntasks={}".format(selfobj.num_proc))

    # set memory by using temperature, time-step and particle count
    def calc_memory_limit():
        """ Automatically calculate memory requirements. To be honest this is pretty crap.
        Again and again it gets it very wrong. So probably just hard writing 1GB or 2Gb may
        much better option.
        """

        T = float(conf_modification.read_line(file_name="CONF",
                                            key="Temperature")[0])
        Tau = float(conf_modification.read_line(file_name="CONF",
                                                key="Tau")[0])
        PPsize = int(conf_modification.read_line(file_name="CONF",
                                                key="PPsize")[0])
        num_prt = read_particles.dynamic_particle_count()

        blocking = read_particles.detect_node_surfaces()

        # magical (empirical) constant got from thin air, that gives enough memory
        c = 0.3  # (0.3 is also good)

        # empirical guess for memory reservation
        mem = (   0.3 +
                c *
                (1 / (T * Tau)) *
                num_prt *
                (1 + 0.2 * int(blocking)))
        mem = min(mem, 4.0)  # 4GB maximum
        mem = max(mem, 0.999)  # 1GB minimum
        mem = round(mem * 1000)

        with open("run_info.txt", "w") as f:
            f.write("pimc3-stuff:\nT = {}\nTau = {}\nnum_prt = {}\nPPsize = {}\nmem_resrv = {}\n\n"
                    .format(T, Tau, num_prt, PPsize, mem))
        #subprocess.Popen(['echo "T = {}\nTau = {}\n num_prt = {}\n PPsize = {}\mem_resrv = {}"'
                     #.format(T, Tau, num_prt, PPsize, mem), ">>", "mem_resrv.txt"], cwd=os.getcwd())
        return mem


    mem = calc_memory_limit()
    conf_modification.replace_line(file_name="SBATCH.sh",
                                    line="#SBATCH --mem-per-cpu=",
                                    replacement=
                                    "#SBATCH --mem-per-cpu={}".format(mem))





"""
Now unused functions which might be helpfull to copypaste and use in variate_value():



# Put fixed particles in positions
#
# Usage:
#     positions = [[-num2/2, 0, 0], [num2/2, 0, 0]]
#     set_prt_positions(positions, prt_name="p", file_name="paths/001.h5")
#
def set_prt_positions(positions, prt_name="p", file_name="paths/001.h5"):
    import read_data.utilities.write_paths_of_fixed_particles as write_paths

    num_species = int(conf_modification.read_line("PARTICLES", key=prt_name)[0])

    if len(positions) != num_species:
        raise Exception("Uh oh, number of particles does not match conf-file.")

    #write_paths.create_paths(positions, prt_name="p", file_name="paths/001.h5")
    write_paths.overwrite_paths_to_fixed_positions(positions, prt_name="p", file_name="paths/001.h5")


# Creates different qca-arrangement for different values of i
#
def qca_configuration(i: int):
    if (i == 0):
        #conf_modification.replace_line("GRID_CONF", key="|DC|",
        #                               replacement="|DC|", range=(57, 62))
        pass
    elif (i == 1):
        conf_modification.replace_line("GRID_CONF", key="|DC|",
                                       replacement="| C|\n|D |", range=(57, 62))
    else:
        raise Exception("Out-of-bonds!")

    #rivi 57-62 "|DC|"


"""
