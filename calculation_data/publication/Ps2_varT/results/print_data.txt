In directory /media/alpi/varalevy/Tietokoneen_tiedostot_backupit/ubuntu_18.04/PC/laskut/kesä_2020/tunarointi/syksy_2021/Ps2_varT
flatpad 2021-11-27T19:29:18



Simulation blocks:
    \tau T|T100.0       |T300.0       |T500.0       |
    -------------------------------------------------
    tau0.1|s516/4090     s335/2560     s265/2058     
    tau0.2|s606/4678     s288/2285     s254/1992     
    tau0.3|s473/3734     s388/2939     s254/1905     
Notation above:
    s<S>/<N>': all success
where N is total number of blocks and S is convergence point.
All observable averages are calculated from blocks S-N.


Energy /E/Et
    \tau T|T100.0      |T300.0      |T500.0      |
    ----------------------------------------------
    tau0.0|-0.5155(4)  |-0.5144(5)  |-0.5132(7)  |
    tau0.1|-0.51579(29)|-0.5148(4)  |-0.5136(5)  |
    tau0.2|-0.51568(18)|-0.51488(29)|-0.51388(29)|
    tau0.3|-0.51620(16)|-0.51538(19)|-0.51434(23)|


Energy /E/Vt
    \tau T|T100.0      |T300.0      |T500.0      |
    ----------------------------------------------
    tau0.0|-0.51578(8) |-0.51581(10)|-0.51575(14)|
    tau0.1|-0.51602(6) |-0.51602(6) |-0.51599(9) |
    tau0.2|-0.51619(4) |-0.51621(7) |-0.51624(8) |
    tau0.3|-0.51648(6) |-0.51643(7) |-0.51648(9) |

