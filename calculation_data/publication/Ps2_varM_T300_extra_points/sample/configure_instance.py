import numpy as np
import subprocess
import os
import datetime
import time
from math import log10, floor

import miscellaneous.conf_modification as conf_modification
import miscellaneous.change_priority as change_priority
from start_PIMC.misc import generate_directory_names
import read_data.utilities.read_particles as read_particles

help_text = """
This is portable file that is used to create full pimc3-calculation set of
variating parameter value. It s used to initialize the set of calculation for
'run_set'.

Usage:
    (0. If this file is 'configure_instance_SAMPLE.py', rename it to
        'configure_instance.py')

    1. modify 'configure_instance.py' to variate some initial value of simulation
        - Most important parameters are in 'InitialSetup'-class, change them.
        - When changing what parameters are varied, modify
            - self.latex_symbol1 &self.latex_symbol2
            - self.latex_unit1 & self.latex_unit2
            - self.range1 & self.range2
            - variate_value()-function

    2. make sure directory tree looks like following, by moving this file:
        sample
            configure_instance.py     <-- this current file
            PARTICLES
            CONF
            SBATCH.sh                 (required even though ran locally)

    3. Run in parent directory that contains directory `sample`
       $ python3 -m start_PIMC.run_set --start

Automatic stuff this does:
    - Main purpose: Generate multiple simulations with varying parameter.
    - If temperature or/and time step is varied, then scale time limit to match
      complexity of simulation. That is, more complex simulations are given
      more time.
    - Scale "Interval" and "NumMoves" parameters to match complexity of simulation
    - In addition to starting new simulations, handle also continuing old ones

"""


class InitialSetup():

    def __init__(self):
        # first variating value
        self.latex_symbol1 = "\\tau"  # Symbol is used in plots, e.g. "\\tau"
        self.latex_unit1 = ""       # e.g. "nm"
        self.range1 = [0.1, 0.2, 0.3]

        # second variating value
        self.latex_symbol2 = "m"
        self.latex_unit2 = "m_p"
        m_e = 1.0/1836.152804
        self.range2 = [m_e*2, m_e*4]

        # The following fields are mostly for sbatch jobs
        self.exe_args = "minimal"   # applies only for sbatch jobs
        # num_proc and mem_per_cpu modifies SBATCH.sh
        self.num_proc = 24
        self.mem_per_cpu = 999  # Megabytes
        # Timelimit is replaced both in CONF and SBATCH.sh
        # If either of latex_symbols is "\\tau" or "T", then timelimit is scaled accordingly
        self.timelimit = 12
        # CSC Puhti billing per hour:  BUs = num_proc + mem_per_cpu * num_proc * 0,1

        # Test if this is run on taito-cluster or just locally on home pc.
        if os.path.isdir("/users/tolvane2"):    # Puhti-cluster
            self.sbatch = True
            # name of executable, full path (or executable must be found from $PATH)
            self.executable = "/users/tolvane2/koodit/pimc3/build/pimc3"
        else:   # Local home computer
            self.sbatch = False
            # Select executable according which personal computer I am using
            if os.path.isdir("/home/alpi/Vain_loota/"):
                self.executable = "/home/alpi/Vain_loota/ohjelmat/pimc3/build/pimc3"
            elif os.path.isdir("/home/alpi/Vain_flatpad/"):
                self.executable = "/home/alpi/Vain_flatpad/ohjelmat/pimc3/build/pimc3"
            else:
                self.executable = "NO BINARY"
                raise Exception("You must set pimc-binary here in configure_instance.py")


        # All following fields are optional, and they can be left out if not used

        self.logarithmic_y_axis = False  # Warning: negative values behave badly
        self.logarithmic_x_axis = False
        self.extrapolate_range1 = True   # Extrapolate range1 at 0
        self.partial_susceptibilites = True

        # Plot reference values in figures. The correct figure is matched with y-axis label name.
        # Yes, it is a hack, but works. (You can try to dig correct y-label name from source code.)
        self.reference_values = {
            #"$\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$" : [("BO, 0K", -2.9858e-11)],
            #"$E_{virial}$ ($E_h$)" : [("BO analytic", -0.5)]
        }

        # ------------------------------------------------------------
        # Other declarations, that are not needed to modify in any way.
        # ------------------------------------------------------------

        # Field 'no_run' will be later overwritten to True, if flag --no-run is passed when
        # starting new simulation.
        self.no_run = False

        # Field 'overwrite_list' will be overwritten when continuing simulation with flag
        # --overwrite, which updates some files. Then 'overwrite_list' contains those files.
        # For example if you decide to change temperature for all calculations, run with flag
        # "--overwrite CONF", which in turn make 'overwrite_list=["CONF"]'. (If you just want
        # to continue without changing any parameters, you can run with just "--continue").
        # All in all, long story short, you should not touch this field and not worry about it.
        self.overwrite_list = "new_run"  # will be modified later in overwrite mode

        # Parent directory name is used as a name for plot output files
        self.parent_dir_name = os.path.basename(os.path.dirname(os.path.dirname(__file__)))

        # Other fields will be later added to this object, but they are used only internally. This
        # object will be called 'init_setup' on the main code side. If you are interested to see
        # what other fields this object will contain, see plot_data/main.py: add_default_values()

    """
    The following function is entry point from `start_PIMC.run_set`.
    """
    def set_up_simulation(self,
                          idx1: int,
                          idx2: int,
                          continuerun=False) -> None:
        num1 = self.range1[idx1]
        num2 = self.range2[idx2]
        """
        By running
            python3 -m start_PIMC.run_set --start
        sample-directory is copied for every simulation, and this function is called to do all the
        modifications to vary parameters.

        This function is called also with commands
            python3 -m start_PIMC.run_set --continue
            python3 -m start_PIMC.run_set --overwrite
        but then `continuerun` is True. With --overwrite flag, `self.overwrite_list` contains
        overwritable file names.

        This function is called per every differing configuration of calulation. So it is
        called from `start_PIMC.run_set` something like so:

        for num1 in InitialSetup.range1
            for num2 in InitialSetup.range2
                set_up_simulation(num1, num2)
        """

        # Make sure at least temperature and tau are not changed without
        # explisit modifications in 'overwrite_list'
        self.check_overwrite_list(num1, num2, continuerun)

        # variate simulation setup somehow for each value
        # (write your own custom function for that)
        self.variate_value(num1, num2)

        modify_CONF_file(self, idx1, idx2, continuerun)
        # Modify sbatch file on puhti
        modify_SBATCH_file(self, idx1, idx2, continuerun)

        # calls correct executable (desktop or sbatch job)
        self.run_pimc3(idx1, idx2)

    def variate_value(self, num1, num2) -> None:
        """
        This function does modifications to copy of simulation template. Two variating values
        are changed.

        :param num1:    variating value on first directory-level
        :param num2:    variating value on second directory-level
        """
        if (self.overwrite_list == "new_run") or ("CONF" in self.overwrite_list):
            modify_Tau(num1, self.latex_symbol1)
            modify_nucleus_mass(num2, self.latex_symbol2)

        print("Running: {}={},  {}={}".format(self.latex_symbol1, num1,
                                              self.latex_symbol2, num2))

    def check_overwrite_list(self, num1, num2, continuerun):
        """ Make some checks whether some most common parameters are changed in
        the middle of calculation, and throw error if required filed are not in
        overwrite_list.

        Now, when I think, this is quite useless function, if not expanded.
        Why would anyone change T or Tau in mid calculation, if dimensions can
        not match?
        TODO add more checks, or actually, compare new and old file in run_set.py
        """

        if not continuerun:
            if self.overwrite_list != "new_run":
                raise Exception('New calculations must have: self.overwrite_list = "new_run"')#
        else:
            if self.overwrite_list == "new_run":
                raise Exception("Please state explisitly that you are not modifying any parameter on this\n"
                                + "continued run. So use flag --continue or --overwrite instad of --start.")

            for num, latex_symbol in ((num1, self.latex_symbol1), (num2, self.latex_symbol2)):
                # check if value is temperature
                allowed_T = ["T"]
                if (latex_symbol in allowed_T):
                    T = float(conf_modification.read_line("CONF", key="Temperature")[0])
                    if (np.abs(num-T) > 0.001) and "CONF" not in self.overwrite_list:
                        raise Exception("You modified temperature, add 'CONF' in 'overwrite_list'")

                # check if value is time step
                allowed_tau = ["Tau", "tau", "\\tau"]
                if (latex_symbol in allowed_tau):
                    T = float(conf_modification.read_line("CONF", key="Tau")[0])
                    if (np.abs(num-T) > 0.001) and "CONF" not in self.overwrite_list:
                        raise Exception("You modified Tau, add 'CONF' in 'overwrite_list'")


    def run_pimc3(self, idx1, idx2):

        if self.no_run:
            subprocess.Popen(["echo", "not running", self.executable, "or anything"],
                             cwd=os.getcwd())
            return

        if self.sbatch:
            subprocess.Popen(["sbatch", "SBATCH.sh"], cwd=os.getcwd())

        else:
            # command = "stdbuf -o L " + self.executable + " " + self.exe_args + " 2>&1 | tee stdout.txt"
            # command = "stdbuf -e L -o L {} {} |& tee mylog".format(self.executable, self.exe_args)
            command = self.executable  # + " " + self.exe_args

            print("Running terminal, command: " + command)
            subprocess.Popen(["konsole", "--noclose", "--new-tab", "--workdir",
                              os.getcwd(), "-e",
                              command],  # "echo Hai! ", command, " && ",
                             cwd=os.getcwd())

            if idx1==len(self.range1)-1 and idx2==len(self.range2)-1:
                time.sleep(1)
            change_priority.main(["asdf", self.executable, "low"])


if __name__ == "__main__":
    raise Exception(
        "No, don't run this. Run 'run_set.py', which uses this as a configuration file.")


def modify_Tau(num, latex_symbol):
    allowed = ["Tau", "tau", "\\tau"]
    if not (latex_symbol in allowed):
        raise Exception("You are modifying incorrect value. '{}' is not in {}."
                        .format(latex_symbol, allowed))

    num = np.around(num, decimals=5)  # prevent numbers like 1.0000000000003 in CONF

    conf_modification.old_bash_style_config_modifier(
        ["-", "-f", "CONF", "--Tau=" + str(num)])

def modify_T(num, latex_symbol):
    allowed = ["T"]
    if not (latex_symbol in allowed):
        raise Exception("You are modifying incorrect value. '{}' is not in {}."
                        .format(latex_symbol, allowed))

    num = np.around(num, decimals=5)
    # change temperature
    conf_modification.old_bash_style_config_modifier(
        ["-", "-f", "CONF", "--Temperature=" + str(num)])

def modify_nucleus_mass(num, latex_symbol):
    allowed = ["m", "m_p"]
    if not (latex_symbol in allowed):
        raise Exception("You are modifying incorrect value. '{}' is not in {}."
                        .format(latex_symbol, allowed))
    m_p = 1836.152804
    num_str = str(np.around(num*m_p, decimals=5))
    # Modifies proton mass "1836.152804" in PARTICLES line:
    # p	2	1.0	1836.152804	5	0
    conf_modification.modify_line("PARTICLES", key="p", word_idx=2, replacement=num_str)


def modify_CONF_file(selfobj, idx1, idx2, continuerun):
    """
    Scale Interval and NumMoves from trotter number.
        Interval ~ Trotter_approx // 100
        NumMoves ~ max(Trotter_approx, NumMoves)
    """
    if (selfobj.overwrite_list != "new_run") and not ("CONF" in selfobj.overwrite_list):
        return
    T = float(conf_modification.read_line(file_name="CONF",
                                            key="Temperature")[0])
    Tau = float(conf_modification.read_line(file_name="CONF",
                                            key="Tau")[0])
    NumMoves = int(conf_modification.read_line(file_name="CONF",
                                            key="NumMoves")[0])
    kelvin_in_au = 1/315775  # This is approximatively one kelvin in atomic units
    trotter = 1 / (T * kelvin_in_au * Tau)
    # trotter_with_one_significant_figure
    Trotter_approx = int(round(trotter, -int(floor(log10(abs(trotter))))))
    conf_modification.old_bash_style_config_modifier(
        ["-", "-f", "CONF", "--Interval=" + str(max(Trotter_approx // 100, 1))])
    conf_modification.old_bash_style_config_modifier(
        ["-", "-f", "CONF", "--NumMoves=" + str(max(Trotter_approx, NumMoves))])

def modify_SBATCH_file(selfobj, idx1, idx2, continuerun):
    """
    Modifies sbatch file for taito cluster.
    Note! This must be called after all modifications on CONF file.
    """
    sbatch = "SBATCH.sh"

    if (selfobj.overwrite_list != "new_run") and not (sbatch in selfobj.overwrite_list):
        return
    num_form1 = selfobj.str_range1[idx1]
    num_form2 = selfobj.str_range2[idx2]
    conf_modification.replace_line(file_name=sbatch,
                                   line="#SBATCH --job-name=",
                                   replacement="#SBATCH --job-name={}_{}_{}".format(
                                       selfobj.parent_dir_name, num_form1,
                                       num_form2))
    # set time limit
    timelim = selfobj.timelimit
    for sym, rang, idx in [(selfobj.latex_symbol1, selfobj.range1, idx1),
                           (selfobj.latex_symbol2, selfobj.range2, idx2)]:
        if sym in ["\\tau", "T"]:  # Inverse scaling to complexity of calculations
            reference = min(rang)
            timelim *=  np.sqrt(reference/rang[idx])
        if sym in ["m"]:  # smaller masses are easier to calculate
            reference = max(rang)
            timelim /=  np.sqrt(np.sqrt(reference/rang[idx]))
    timelim = int(np.ceil(timelim))
    extra_minutes = 0

    if timelim <= 72:
        # Adds extra hour margin because PIMC goes sometimes a bit overtime
        if timelim < 72:
            extra_minutes = 30
        conf_modification.replace_line(file_name=sbatch,
                                    line="#SBATCH --time=",
                                    replacement="#SBATCH --time={:02}:{:02}:00"
                                    .format(timelim, extra_minutes))
    else:
        # BTW, you need longrun partition for jobs over three days (72 hours)
        if timelim < 336:
            extra_minutes = 30
        conf_modification.replace_line(file_name=sbatch,
                                    line="#SBATCH --partition=",
                                    replacement="#SBATCH --partition=longrun")
        conf_modification.replace_line(file_name=sbatch,
                                    line="#SBATCH --time=",
                                    replacement="#SBATCH --time={:1}-{:02}:{:02}:00"
                                    .format(timelim // 24, timelim % 24, extra_minutes))
    conf_modification.old_bash_style_config_modifier(
        ["-", "-f", "CONF",
        "--TimeLimit={}".format(timelim)])

    time = datetime.datetime.now().replace(microsecond=0).isoformat()[:-3]

    # Set output file names to contain date (this prevents overwrite if calculation is continued)
    conf_modification.replace_line(file_name=sbatch, line="#SBATCH -o",
                                   replacement="#SBATCH -o out_{}.txt".format(time))
    conf_modification.replace_line(file_name=sbatch, line="#SBATCH -e",
                                   replacement="#SBATCH -e err_{}.txt".format(time))

    # Set executable
    if selfobj.sbatch == True:
        conf_modification.replace_line(file_name=sbatch,
                                    line="srun",
                                    replacement="srun {} {}".format(
                                        selfobj.executable, selfobj.exe_args))

    # This sets number of used processors i.e. MPI processes i.e. slurm tasks.
    # Note: pimc3 achieves parallelism with MPI processess, not with threads.
    # (Well ok, there is thread support with OpenMP, but MPI processes work a bit better.)
    conf_modification.replace_line(file_name="SBATCH.sh",
                                    line="#SBATCH --ntasks=",
                                    replacement="#SBATCH --ntasks={}".format(selfobj.num_proc))

    conf_modification.replace_line(file_name="SBATCH.sh",
                                    line="#SBATCH --mem-per-cpu=",
                                    replacement=
                                    "#SBATCH --mem-per-cpu={}".format(selfobj.mem_per_cpu))





"""
Now unused functions which might be helpfull to copypaste and use in variate_value():



# Put fixed particles in positions
#
# Usage:
#     positions = [[-num2/2, 0, 0], [num2/2, 0, 0]]
#     set_prt_positions(positions, prt_name="p", file_name="paths/001.h5")
#
def set_prt_positions(positions, prt_name="p", file_name="paths/001.h5"):
    import read_data.utilities.write_paths_of_fixed_particles as write_paths

    num_species = int(conf_modification.read_line("PARTICLES", key=prt_name)[0])

    if len(positions) != num_species:
        raise Exception("Uh oh, number of particles does not match conf-file.")

    #write_paths.create_paths(positions, prt_name="p", file_name="paths/001.h5")
    write_paths.overwrite_paths_to_fixed_positions(positions, prt_name="p", file_name=file_name)


# Calculate memory requirements (Crap, better constants needed)
def calc_memory_limit():
    # Automatically calculate memory requirements. To be honest this is pretty crap.
    # Again and again it gets it very wrong. So probably just hard writing 1GB or 2Gb may
    #much better option.

    T = float(conf_modification.read_line(file_name="CONF",
                                        key="Temperature")[0])
    Tau = float(conf_modification.read_line(file_name="CONF",
                                            key="Tau")[0])
    PPsize = int(conf_modification.read_line(file_name="CONF",
                                            key="PPsize")[0])
    num_prt = read_particles.dynamic_particle_count()

    blocking = read_particles.detect_node_surfaces()

    # magical (empirical) constant got from thin air, that gives enough memory
    c = 0.3  # (0.3 is also good)

    # empirical guess for memory reservation
    mem = (   0.3 +
            c *
            (1 / (T * Tau)) *
            num_prt *
            (1 + 0.2 * int(blocking)))
    mem = min(mem, 4.0)  # 4GB maximum
    mem = max(mem, 0.999)  # 1GB minimum
    mem = round(mem * 1000)

    with open("run_info.txt", "w") as f:
        f.write("pimc3-stuff:\nT = {}\nTau = {}\nnum_prt = {}\nPPsize = {}\nmem_resrv = {}\n\n"
                .format(T, Tau, num_prt, PPsize, mem))
    #subprocess.Popen(['echo "T = {}\nTau = {}\n num_prt = {}\n PPsize = {}\mem_resrv = {}"'
                    #.format(T, Tau, num_prt, PPsize, mem), ">>", "mem_resrv.txt"], cwd=os.getcwd())
    return mem
"""
