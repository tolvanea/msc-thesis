"""
This file calculates most of susceptibility reference values of the thesis.
This file is written for personal use only and it is not very readable or clear.
However, it is theoretically possible to replicate presented reference values with this script.

Also see the script in publication, which is updated version of this
https://gitlab.com/tolvanea/thesis_publication/-/blob/master/scripts/reference_value_conversions.py

"""
import numpy as np

class si:
    e=1.602176620898e-19;
    hbar = 6.6260755e-34/(2*np.pi);
    eps0=8.85419e-12;
    m_e=9.1093897e-31;      # elektronin massa
    m_p=1.6726231e-27;      # protonin massa
    µ_0 = 4*np.pi * 1e-7
    a0 = 5.291772109e-11

    # Unitless:
    alpha = 1/137.035999084
    N_A = 6.02214076e23

class au:
    e = 1.0
    hbar = 1.0
    eps0 = 1.0/(4*np.pi)
    m_e = 1.0
    m_p = 1836.1526734
    c = 137.035999084
    µ_0 = 4*np.pi / c**2
    a0 = 1.0

    # Unitless:
    alpha = 1.0/c
    N_A = 6.02214076e23


from_au_to_SI = si.µ_0* si.e**2 * si.a0**2 / si.m_e * si.N_A
from_cgs_to_si = 1e-6 * 4*np.pi

# A NOTE ON THE MAGNETIC SUSCEPTIBlLlTY OF HYDROGEN AND 1TS ISOTOPOMERS
# Brain dead stupid units. Oh lord why, Raynes, why?
raynes_units = 4.0051e-6 / 1.6855 * from_cgs_to_si

def E_to_susc(E_dia):
    # Transform values from Rebane's paper's au energy units to si susceptibility

    chi_au = (-2 * au.alpha**2 * E_dia)
    chi_SI = (4 * np.pi * si.a0**3 * si.N_A) * chi_au
    return chi_SI
"""
# Test whether I get same result with eq 34 as with analytical H BO equation. Answer: yes.
# Susc. of H with rebanes eq 34
#    <r^2> = 3a_0^2
#    E_dia: e^2 * (3* a0^2) / (12 * c^2 * m_e)
#         = e^2 * a0^2 / (4 * c^2 * m_e)
#    chi_dia: -2 * E_dia = -e^2 * a0^2 / (2 * c^2 * m_e)
#                       = -e^2 * a0^2 / (2 * m_e) * (1/c^2)
# Susc of H in my thesis:
#    -e^2 * a0^2 / (2m_e) * mu_0
# Eq 34 and my thesis' eq. match, as match as it should
# Also to keep in mind:
# - alpha in au = 1/c
#   alpha = mu_0 * e^2 * c / (4*pi* hbar)
# - X_SI = 4pi*X_gaussian
"""

def a_const(c, m):
    # If m=m_e, then this equal to bohr radius a0. Otherwise when m is reduced mass, 'a' is mean
    # electron radius <r> of AQ hydrogen atom. With this can be calculated squared mean <r^2> also
    # See p. 88 in Atkins: "Molecular Quantum Mechanics", fift edition 2011
    # \cite[p.~88]{atkinsMolecularQuantumMechanics}
    return 4 * np.pi * c.eps0 * c.hbar**2 / (m * c.e**2)

def m_r(m1, m2):
    # Reduced mass
    return 1 / (1/m1 + 1/m2)

def r2_mean(a):
    # 'a' is mean radius of nucleai and electron <r>. This gives squared mean
    # radius of nucleai and electron <r^2>. This can be derived on paper by
    # integration. Integral B14 on by thesis
    return 3 * a**2

def H_susceptibility(c, m1, m2, random_testing=False):
    """Calculates analytical value of susceptibility of H
    BO: m1=m_e, m2=inf
        - I derived this in my thesis appendix
    AQ: m1=m_e, m2=m_p
        - Derived from Rebane's equation 34.
    """
    reduced_mass = m_r(m1, m2)
    a = a_const(c, reduced_mass)
    chi = -c.µ_0 * c.e**2/(2*reduced_mass) * a**2
    if c is si:
        chi *= c.N_A  # per mole
    return chi

def eq_34(c, p, N, q, m, M, r2_1N, r2_12, r2_N1N):
    """
    Calculate magnetic susceptibilities for molecules, with and without born oppenheimer.
    See  article "Nonadiabatic theory of diamagnetic susceptibility of molecules", Rebane 2002
    Use atomic units in inputs.
    Args
        p       number of electrons
        N       number of nucleus + electrons
        q       charge of electron
        m       mass of electron
        M       nucleus mass
        r2_1N   square mean distance <r^2> of nucleus-electron
        r2_12   square mean distance <r^2> of electron-electron
        r2_N1N  square mean distance <r^2> of nucleus-nucleus
        """
    alpha_units = 1/(c.alpha**2)
    E_dia = alpha_units \
        * (p*q**2/(24*(N-p)*c.c**2)) \
        * ((1/m)*(2*(N-p)*r2_1N - (N-p-1)*r2_N1N) \
        + (1/M)*(2*p*r2_1N - (p-1)*r2_12))
    return E_to_susc(E_dia)


# For H2 BO
def rotational_average(values, angles=None, warn=True):
    """
    Rotationally average non-isotropic observables of diatomic BO systems.
    Assumes random and independent orientation of molecule.
    That is, if susceptibility is calculated with angles [0, pi/4, pi/2] to H2 bond,
    this function calculates the average with weighting of spherical sectors.
    This is thermal average.
    Angle=0 is parallel with line connecting nucleai, and pi/2 is perpendicular.
    Remember that parallel angle between the bond and the axis is less common than
    perpendicular angle.
    """
    def sector_area(phi_rad, R):
        # Area of circular sector on sphere located at origin.
        # 'phi_rad' is angle (in radians) between y.axis and a sector. That is,
        #     phi_rad=0         yields zero area
        #     0<phi_rad<pi/2    yields the area of a cap
        #     phi_rad=pi/2      yields area of half sphere.
        # https://en.wikipedia.org/wiki/Spherical_sector
        # https://en.wikipedia.org/wiki/Steradian
        Omega = (2 * np.pi * (1 - np.cos(phi_rad)))
        return Omega * R**2
    area = 0.0
    exp_val = 0.0
    if angles==None:
        angles = np.linspace(0, np.pi/2, len(values))
    if warn and values[0]**2 > values[-1]**2:
        raise Exception("Have you parallel value first in list? (You should.)")
    assert(len(values) == len(angles))
    assert(np.isclose(angles[0], 0.0) and np.isclose(angles[-1], np.pi/2))
    for i in range(len(values)):
        # Midways between the two angles
        phi_inner = (angles[i] + angles[max(i-1, 0)]) / 2
        phi_outer = (angles[i] + angles[min(i+1, len(angles)-1)]) / 2

        # Sphere surface is divided into horizontally oirented circular stripe loops
        A_of_stripe = sector_area(phi_outer, 1) - sector_area(phi_inner, 1)
        area += A_of_stripe
        exp_val += A_of_stripe * values[i]
    exp_val /= area
    assert(np.isclose(area, 4*np.pi/2))  # Area is surface of half sphere
    return exp_val

# For H2 BO
def ellipsoid_rotational_average(parallel, perpendicular, warn=True):
    """
    Function 'flat_rot_avg()' is correct, this is not.

    This is a version of 'rotational_average()', that does interpolation and does
    integration by only using parallel and perpendicular components of susceptibility.

    This assumes that anisotropic susceptibility of a rotationally symmetric molecule
    is like and ellipsoid, that is, susceptibility to some direction d is just
    can be calculated with a dot product with parallell and perpedicular components.
    """
    # integration grid, by symmetry only half sphere considered
    phis = np.linspace(0, np.pi/2, 1000)
    if warn and parallel**2 > perpendicular**2:
        raise Exception("Did you accidentally swap the arguments?")

    def stripe_area(phi_low, phi_high):
        # Area of a latitude stripe of a sphere. Phi=0 is pole
        # Derived from 'sector_area(phi_high) - sector_area(phi_low)', with R=1
        # Applied trigonometric formula: cos(x)-cos(y) = -2*sin((x+y)/2) * sin((x-y)/2)
        trig = -2 * np.sin((phi_low + phi_high) / 2) * np.sin((phi_low - phi_high) / 2)
        return trig * 2 * np.pi

    # Weighting so that parallel angles are less propable
    weights = stripe_area(phis[:-1], phis[1:])
    ys = np.cos(phis[1:]) * parallel
    xs = np.sin(phis[1:]) * perpendicular
    radii = np.sqrt(xs**2 + ys**2)  # distance from center to surface of ellipsoid
    integral = (weights * radii).sum() * np.sign(perpendicular)
    return integral / (2*np.pi)  # Division by area of half sphere

# For H2 BO
def flat_rot_avg(parallel, perpendicular, warn=True):
    # THIS IS THE CORRECT ROTATIONAL AVERAGE! THE TWO ABOVE ARE NOT.
    #
    # Knowing parallel and perpedicular components of susceptibility in
    # H2 (BO), calculate rotational average (3d) in bulk system. This
    # equation is simply the mean (X_x + X_y + X_z) / 3

    # This is exact rotational average, which would not hold for simple
    # classical components. The reason stems from <m^2> nature of
    # susceptibility, see
    #     https://doi.org/10.1021/ed081p877
    #     https://www.smoldyn.org/andrews/papers/Andrews_2004.pdf
    #
    # This equation is widely used, for example it is used in eq.1 of
    #     TITLE:    A note on the magnetic susceptibility of hydrogen and its isotopomers
    #     BIBTEX:   raynesNoteMagneticSusceptibility1974
    #     URL:      https://www.sciencedirect.com/science/article/abs/pii/0009261474802368
    if warn and parallel**2 > perpendicular**2:
        raise Exception("Did you accidentally swap the arguments?")
    return 1/3 * (parallel + 2* perpendicular)


def susceptibility_reference_values():
    """Calculates all susceptibility reference values"""
    print("Susceptibility reference values in All Quantum (AQ) and Born Oppenheimer (BO) systems\n")
    print("Used notations:")
    print("'r^2' means that numeric r^2 value is taken from following source,")
    print("      and susceptibility is by rebane's equation")
    print("'ref' means numeric reference value is taken from following source,")
    print("      and rebane's equation is not used.\n\n")

    def print_H():
        chi_H_AQ =  E_to_susc(0.250409)  # Direct number from rebanes paper

        # Derived with equation 34 of Rebanes paper. Using analytical <r^2>
        chi_H_AQ_v2 = H_susceptibility(si, si.m_e, si.m_p)

        print("χ_H AQ, ref direct rebane    ", "{:.6e}".format(chi_H_AQ))
        print("χ_H AQ, analyt. <r^2>+rebane ", "{:.6e}".format(chi_H_AQ_v2))
        if False:
            # Verification:
            # The previous result 'chi_H_AQ_v2' is exactly same calculation as:
            H_r2 = r2_mean(a_const(au, m_r(au.m_e, au.m_p)))
            # Equation 34 of Rebanes paper
            chi_H_AQ_v3 = eq_34(
                au, p=1, N=2, q=-au.e, m=au.m_e, M=au.m_p,
                r2_1N=H_r2,
                r2_12=0.0,
                r2_N1N=0.0
            )
            print("χ_H AQ, r^2 analytic         ", "{:.12e}".format(chi_H_AQ_v3))
        print("--")
        #
        #-------------------
        #
        chi_H_BO = eq_34(
            au, p=1, N=2, q=-au.e, m=au.m_e, M=np.inf,
            r2_1N=r2_mean(a_const(au, au.m_e)),
            r2_12=0.0,
            r2_N1N=0.0
        )

        # Derived in my thesis
        chi_BO_analytical = H_susceptibility(si, si.m_e, float("inf"))
        if False:
            # Warning, due to the floating point conversions, no more than 5 decimals should be used!!!
            # Good example is to calculate above result again in atomic units, and convert it to SI
            # Can bee seen that 5th decimal differs as does with comparison with 'chi_H_BO'.
            same_in_au = H_susceptibility(au, au.m_e, float("inf"))
            chi_BO_analytical_v2 = same_in_au/au.µ_0*from_au_to_SI
            print("TWO FOLLOWING VALUES ARE THE SAME THING WITH BUT UNIT CONVERSIONS AT DIFFERENT ORDER")
            print("χ_H BO, r^2 analytical           ", "{:.10e}".format(chi_BO_analytical_v2))

        print("χ_H BO, r^2 analytical       ", "{:.5e}".format(chi_H_BO))
        print("χ_H BO, analytical           ", "{:.5e}".format(chi_BO_analytical))
    print("H ---------------------")
    print_H()
    print("")

    def print_He():
        from uncertainties import ufloat
        chi_He_AQ = E_to_susc(0.198939)
        # Values from original paper
        chi_He_AQ_v2 = eq_34(
            au, p=2, N=3, q=-au.e, m=au.m_e, M=7294.299335*au.m_e,
            r2_1N=1.1934830 * au.a0**2,
            r2_12=2.5164393 * au.a0**2,
            r2_N1N=0.0
        )
        # Values from: https://sci-hub.st/https://journals.aps.org/pra/abstract/10.1103/PhysRevA.38.5995
        # \cite{haftelPreciseNonvariationalCalculations1988}
        chi_He_AQ_ref2 = eq_34(
            au, p=2, N=3, q=-au.e, m=au.m_e, M=7294.299335*au.m_e,
            r2_1N=1.19383502 * au.a0**2,
            r2_12=2.517062655 * au.a0**2,
            r2_N1N=0.0
        )
        # ref: DIAMAGNETIC SUSCEPTIBILITIES OF SIMPLE HYDROCARBONS AND VOLATILE HYDRIDES
        # \cite{barterDIAMAGNETICSUSCEPTIBILITIESSIMPLE1960}
        chi_ref1 = ufloat(-2.02e-6, 0.08e-6) * from_cgs_to_si
        # \cite{willsMagneticSusceptibilityOxygen1924}
        # Actually I cheat, and read the value from willsMagneticSusceptibilityOxygen1924
        chi_ref_hector = -1.88e-6 * from_cgs_to_si
        # havensMagneticSusceptibilitiesCommon1933
        chi_ref_havens = -1.906e-6 * from_cgs_to_si
        print("χ_He AQ ref rebane            ", "{:.6e}".format(chi_He_AQ))
        print("χ_He AQ r^2 rebane            ", "{:.7e}".format(chi_He_AQ_v2))
        print("χ_He AQ r^2 haftel            ", "{:.7e}".format(chi_He_AQ_ref2))
        print("χ_He AQ ref barter exp        ", "{:.2e}".format(chi_ref1))
        print("χ_He AQ ref hector (292K) exp ", "{:.2e}".format(chi_ref_hector))
        print("χ_He AQ ref havens exp        ", "{:.2e}".format(chi_ref_havens))



        print("--")
        # (same source as few lines above: 10.1103/PhysRevA.38.5995)
        # \cite{haftelPreciseNonvariationalCalculations1988}
        chi_He_BO_ref2 = eq_34(
            au, p=2, N=3, q=-au.e, m=au.m_e, M=np.inf,
            r2_1N=1.19348312 * au.a0**2,
            r2_12=2.516440118 * au.a0**2,
            r2_N1N=0.0
        )
        # ref: Hartree-Fock Diamagnetic Susceptibilities
        # \cite{mendelsohnHartreeFockDiamagneticSusceptibilities1970b}
        chi_ref1 = -1.8767e-6 * from_cgs_to_si
        # ref: Diamagnetism of helium
        # \cite{bruchDiamagnetismHelium2000}
        chi_ref3 = -1.876e-6 * from_cgs_to_si
        chi_ref4 = -1.890e-6 * from_cgs_to_si
        print("χ_He BO r^2 haftel    ", "{:.7e}".format(chi_He_BO_ref2))
        print("χ_He BO ref mendelson ", "{:.4e}".format(chi_ref1))
        print("χ_He BO ref bruch     ", "{:.3e}".format(chi_ref3))
        print("χ_He BO ref bruch 2   ", "{:.3e}".format(chi_ref4))
    print("He ---------------------")
    print_He()
    print("")

    def print_H2():
        # H2 <r^2> values (0-vibrational state) from:
        # https://nur.nu.edu.kz/bitstream/handle/123456789/1095/kedziera_jcp_125_014318_2006.pdf;jsessionid=6054FB0418727EAB6667B8E76B976703?sequence=1
        #\cite{kedzieraDarwinMassvelocityRelativistic2006}
        chi_H2_AQ = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_p,
            r2_1N=3.14539 * au.a0**2,
            r2_12=5.80533 * au.a0**2,
            r2_N1N=2.12705 * au.a0**2
        )
        # https://sci-hub.tw/https://aip.scitation.org/doi/10.1063/1.2978172
        # \cite{alexanderFullyNonadiabaticProperties2008}
        chi_H2_AQ_juhan = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_p,
            r2_1N=3.1465 * au.a0**2,
            r2_12=5.806 * au.a0**2,
            r2_N1N=2.1273 * au.a0**2
        )
        # \cite[p.~812]{jainDiamagneticSusceptibilityAnisotropy2007}
        chi_exp_ref1 = -3.99e-6 * from_cgs_to_si
        chi_exp_ref2 = -4.01e-6 * from_cgs_to_si
        # ref: Magnetic  Properties  of the  Hydrogen  Molecules
        # \cite{ishiguroMagneticPropertiesHydrogen1954}
        chi_exp_ref3 = -4.0689e-6 * from_cgs_to_si
        # ON THE DIAMAGNETIC SUSCEPTIBILITY OF GASES
        # \cite{glickDIAMAGNETICSUSCEPTIBILITYGASES1961}
        chi_exp_ref4 = -4.04e-6 * from_cgs_to_si

        # A NOTE ON THE MAGNETIC SUSCEPTIBlLlTY OF HYDROGEN AND 1TS ISOTOPOMERS
        chi_AQ_raynes_v0J0 = -1.7367 * raynes_units
        chi_AQ_raynes_0K = -1.7057 * raynes_units
        chi_AQ_raynes_300K = -1.7084 * raynes_units

        # ruudFullCICalculations1996
        chi_ruud_1 = -67.13e-30 * si.µ_0 * si.N_A
        err_ruud_1 = 0.04e-30 * si.µ_0 * si.N_A

        chi_ruud_2 = -67.14e-30 * si.µ_0 * si.N_A
        err_ruud_2 = 0.04e-30 * si.µ_0 * si.N_A

        print("χ_H2 AQ r^2 kedziera            ", "{:.5e}".format(chi_H2_AQ))
        print("χ_H2 AQ r^2 alexander           ", "{:.4e}".format(chi_H2_AQ_juhan))
        print("χ_H2 AQ ref jain exp            ", "{:.2e}".format(chi_exp_ref1))
        print("χ_H2 AQ ref jain exp            ", "{:.2e}".format(chi_exp_ref2))
        print("χ_H2 AQ ref ishiguro (rot. only)", "{:.4e}".format(chi_exp_ref3))
        print("χ_H2 AQ ref glick               ", "{:.2e}".format(chi_exp_ref4))
        print("χ_H2 AQ ref raynes v=0 J=0,     ", "{:.4e}".format(chi_AQ_raynes_v0J0))
        print("χ_H2 AQ ref raynes 0K           ", "{:.4e}".format(chi_AQ_raynes_0K))
        print("χ_H2 AQ ref raynes 300K         ", "{:.4e}".format(chi_AQ_raynes_300K))
        print(
            "χ_H2 AQ ref ruud1               ", "{:.3e}({:.3f}e-11)"
            .format(chi_ruud_1, err_ruud_1*1e11)
        )
        print(
            "χ_H2 AQ ref ruud2               ", "{:.3e}({:.3f}e-11)"
            .format(chi_ruud_1, err_ruud_1*1e11)
        )

        # TODO selitä dipassa miksi BO:n laskeminen Rebanen AQ kaavalla on sallittua.
        #      Eli kun m -> inf, niin nollapistevärähtely -> 0 ja R -> ~1.4

        # https://sci-hub.st/https://aip.scitation.org/doi/abs/10.1063/1.1697142
        # \cite{kol/osPotentialEnergyCurves1965}
        chi_H2_BO_R140 = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=np.inf,
            r2_1N=3.0363543 * au.a0**2,  # e-p
            r2_12=5.6323895 * au.a0**2,  # e-e
            r2_N1N=1.40**2 * au.a0**2    # p-p (constant in BO)
        )
        chi_H2_BO_R145 = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=np.inf,
            r2_1N=3.1364572 * au.a0**2,  # e-p
            r2_12=5.7918526 * au.a0**2,  # e-e
            r2_N1N=1.45**2 * au.a0**2    # p-p (constant in BO)
        )
        chi_H2_BO_R200 = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=np.inf,
            r2_1N=4.3577311 * au.a0**2,  # e-p
            r2_12=7.757747 * au.a0**2,   # e-e
            r2_N1N=2.00**2 * au.a0**2    # p-p (constant in BO)
        )

        # https://sci-hub.tw/https://onlinelibrary.wiley.com/doi/10.1002/qua.21130
        # \cite{alexanderRovibrationallyAveragedProperties2007}
        chi_H2_BO_R140_juhan = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=np.inf,
            r2_1N=3.037 * au.a0**2,  # e-p
            r2_12=5.635 * au.a0**2,  # e-e
            r2_N1N=1.40**2 * au.a0**2   # p-p  (constant in BO)
        )

        # Alijah https://arxiv.org/pdf/1903.08324.pdf
        # \cite{alijahHydrogenMoleculeRmH2019a}
        angles = np.array([(deg / 360) * 2*np.pi for deg in [0, 45, 90]])
        # Extended number of angles for more precise spherical average
        angles_extend = np.linspace(0, np.pi/2, 1000)

        # Use directly written susceptibility values for inclinations, phi = [0, 45, 90] degrees
        suscs_direct = np.array([-0.7647, -0.8258, -0.88700]) * from_au_to_SI
        chi_H2_BO_R140_Alijah = rotational_average(suscs_direct, angles)
        chi_H2_BO_R140_Alijah_bad = rotational_average(suscs_direct[[0,2]], angles[[0,2]])

        # Reproduce susceptibilities from particle distances with http://arxiv.org/abs/1409.6204
        # That equation is for H2+ (one electron), H2 contains two electorns, so times 2 for that
        susc_BO_H2 = lambda x2, z2, phi: -(1/2) * (x2 * (1+np.cos(phi)**2) + z2 * np.sin(phi)**2)
        r2_phi0 = np.array([0.7646, 0.7646, 1.00929])  # [x^2, y^2, z^2] at phi=0
        suscs_recalc = susc_BO_H2(r2_phi0[0], r2_phi0[2], angles) * from_au_to_SI
        suscs_extend = susc_BO_H2(r2_phi0[0], r2_phi0[2], angles_extend) * from_au_to_SI
        chi_H2_BO_R140_Alijah_recalc = rotational_average(suscs_recalc, angles)
        chi_H2_BO_R140_Alijah_extend = rotational_average(suscs_extend, angles_extend)

        # ref: Magnetic Susceptibility of Diatomic Molecules
        # \cite{karplusMagneticSusceptibilityDiatomic1963}
        chi_BO_ref1 = -4.868e-6 * from_cgs_to_si

        # A NOTE ON THE MAGNETIC SUSCEPTIBlLlTY OF HYDROGEN AND 1TS ISOTOPOMERS
        # raynesNOTEMAGNETICSUSCEPTIBlLlTY1974
        suscs_raynes = [-1.457 * raynes_units, -1.677 * raynes_units]
        suscs_raynes2 = [-1.5234 * raynes_units, -1.7847 * raynes_units]
        angles_raynes = [0, np.pi/2]
        chi_BO_raynes_avg = rotational_average(suscs_raynes, angles_raynes)
        chi_BO_raynes2_avg = rotational_average(suscs_raynes2, angles_raynes)
        # Rotational average equation given by raynes himself
        chi_BO_raynes3 = 1/3 * (suscs_raynes[0] + 2*suscs_raynes[1])

        # Magnetic  Properties  of the  Hydrogen  Molecules
        chi_BO_140_lam = lambda th: (-3.5814 + (-0.7339 + 0.1947) * np.sin(th)**2) * 1e-6*from_cgs_to_si
        chi_BO_ISHI_r140 = rotational_average(
            [chi_BO_140_lam(th) for th in angles_extend],
            angles_extend
        )


        print("-")
        print("χ_H2 BO R=1.4  r^2 alexander                  ", "{:.4e}".format(chi_H2_BO_R140_juhan))
        print("χ_H2 BO R=1.40 r^2 kolos                      ", "{:.5e}".format(chi_H2_BO_R140))
        print("χ_H2 BO R=1.40 ref alijah with sphr. avg.     ", "{:.4e}".format(chi_H2_BO_R140_Alijah))
        print("χ_H2 BO R=1.40 recalc alijah better sphr. avg.", "{:.4e}".format(chi_H2_BO_R140_Alijah_extend))
        print("χ_H2 BO R=1.40 recalc alijah worse sphr. avg. ", "{:.4e}".format(chi_H2_BO_R140_Alijah_bad))
        print("χ_H2 BO R=minE ref karplus (dia)              ", "{:.3e}".format(chi_BO_ref1))
        print("χ_H2 BO R=1.40 ref raynes with sphr. avg.     ", "{:.3e}".format(chi_BO_raynes_avg))
        print("χ_H2 BO R=1.40 ref raynes 2 with sphr. avg.   ", "{:.3e}".format(chi_BO_raynes2_avg))
        print("χ_H2 BO R=1.40 ref raynes 3                   ", "{:.3e}".format(chi_BO_raynes3))
        print("χ_H2 BO R=1.40 ref ISHIGURO                   ", "{:.3e}".format(chi_BO_ISHI_r140))
        if False:
            print("other")
            print("χ_H2 BO R=1.40 recalc alijah worse sphr. avg. ", "{:.4e}".format(chi_H2_BO_R140_Alijah_recalc))
            print("χ_H2 BO R=1.45 r^2 kolos                      ", "{:.5e}".format(chi_H2_BO_R145))
            print("χ_H2 BO R=2.00 r^2 kolos                      ", "{:.5e}".format(chi_H2_BO_R200))
            print("χ_H2 BO R=1.40 0°  ref alijah", "direct: {:.4e}, recalc: {:.4e}"
                .format(suscs_direct[0], suscs_recalc[0]))
            print("χ_H2 BO R=1.40 45° ref alijah", "direct: {:.4e}, recalc: {:.4e}"
                .format(suscs_direct[1], suscs_recalc[1]))
            print("χ_H2 BO R=1.40 90° ref alijah", "direct: {:.4e}, recalc: {:.4e}"
                .format(suscs_direct[2], suscs_recalc[2]))

    print("H2 ---------------------")
    print_H2()
    print("")

    def print_HD():
        # A NOTE ON THE MAGNETIC SUSCEPTIBlLlTY OF HYDROGEN AND 1TS ISOTOPOMERS
        # raynesNOTEMAGNETICSUSCEPTIBlLlTY1974
        print("χ_HD AQ ref raynes 0K  ", "{:.3e}".format(-1.7022 * raynes_units))
        print("χ_HD AQ ref raynes 300K", "{:.3e}".format(-1.7050 * raynes_units))

    print("HD ---------------------")
    print_HD()
    print("")

    def print_D2():
        # D2 <r^2> values (0-vibrational state) from:
        # https://nur.nu.edu.kz/bitstream/handle/123456789/1061/bubin_jcp_135_74110_2011.pdf;jsessionid=2747C116E38E9B45B42FF20B649F712E?sequence=1
        # \cite{bubinAccurateNonBornOppenheimer2010}
        chi_D2 = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=3670.4829654*au.m_e,
            r2_1N=3.113364 * au.a0**2,
            r2_12=5.754556 * au.a0**2,
            r2_N1N=2.077687 * au.a0**2
        )
        # \cite{ishiguroMagneticPropertiesHydrogen1954}
        chi_exp_ref1 = -4.0306e-6  * from_cgs_to_si
        print("χ_D2 AQ r^2 bubin        ", "{:.6e}".format(chi_D2))
        print("χ_D2 AQ ref ishiguro     ", "{:.4e}".format(chi_exp_ref1))
        # A NOTE ON THE MAGNETIC SUSCEPTIBlLlTY OF HYDROGEN AND 1TS ISOTOPOMERS
        # raynesNOTEMAGNETICSUSCEPTIBlLlTY1974
        print("χ_D2 AQ ref raynes 0K    ", "{:.3e}".format(-1.6976 * raynes_units))
        print("χ_D2 AQ ref raynes 300K  ", "{:.3e}".format(-1.7004 * raynes_units))
    print("D2 ---------------------")
    print_D2()
    print("")

    def print_T2():
        # https://www.sciencedirect.com/science/article/pii/S0009261410007554
        # \cite{bubinAccurateNonBornOppenheimer2010}
        chi_T2 = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=5496.92158*au.m_e,
            r2_1N=3.099443 * au.a0**2,
            r2_12=5.732488 * au.a0**2,
            r2_N1N=2.056241 * au.a0**2
        )
        print("χ_T2 AQ r^2 bubin       ", "{:.6e}".format(chi_T2))
    print("T2 ---------------------")
    print_T2()
    print("")

    def print_Ps2():
        chi_Ps2_AQ_ref_rebane =  E_to_susc(5.839075)

        # Ps2 <r^2> values from rebane himself
        chi_Ps2_AQ_r2_rebane = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_e,
            r2_1N=29.1093 * au.a0**2,
            r2_12=46.3683 * au.a0**2,
            r2_N1N=46.3683 * au.a0**2
        )

        # https://nur.nu.edu.kz/bitstream/handle/123456789/1070/bubin_pra_75_062504_2007.pdf;jsessionid=F5905C648962E8569B85A2CFF2C24DD6?sequence=1
        # \cite{bubinRelativisticCorrectionsGroundstate2007}
        chi_Ps2_AQ = eq_34(
            au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_e,
            r2_1N=29.11270462 * au.a0**2,
            r2_12=46.37487912 * au.a0**2,
            r2_N1N=46.37487912 * au.a0**2
        )

        print("χ_Ps2 AQ ref rebane    ", "{:.5e}".format(chi_Ps2_AQ_ref_rebane))
        print("χ_Ps2 AQ r^2 rebane    ", "{:.5e}".format(chi_Ps2_AQ_r2_rebane))
        print("χ_Ps2 AQ r^2 bubin     ", "{:.8e}".format(chi_Ps2_AQ))
        
    print("Ps2 ---------------------")
    print_Ps2()
    print("")

    def print_H2plus_and_Hminus():
        # H2+    https://arxiv.org/pdf/1409.6204.pdf
        # \cite{cobaxinMathrmWeakMagnetic2015}
        suscs = np.array([0.32018, 0.32807, 0.34961, 0.37906, 0.40849, 0.43003, 0.43792]) * from_au_to_SI
        angles = [(deg / 360) * 2*np.pi for deg in [0, 15, 30, 45, 60, 75, 90]]

        # https://royalsocietypublishing.org/doi/pdf/10.1098/rspa.1932.0103
        chi_Hm_exp_ref1 = -9.87e-6  * from_cgs_to_si

        # spherical_avg_of_suscebtibility
        print("χ_H2+ BO: ref    ", "{:.5e}".format(rotational_average(suscs, angles)) )
        print("χ_H-  BO: ref    ", "{:.2e}".format(chi_Hm_exp_ref1))
    print("H2+ & H- -------------------")
    print_H2plus_and_Hminus()
    print("")

    def print_Ps():
        chi_Ps = H_susceptibility(si, si.m_e, si.m_e)
        chi_Ps_pimc = eq_34(    # See data -1 at the end of this file for <r^2> data
            au, p=1, N=2, q=-au.e, m=au.m_e, M=au.m_e,
            r2_1N=12.00,    # p-e
            r2_12=0.0,
            r2_N1N=0.0
        )
        print("χ_Ps AQ, rebabe analytic <r^2>    ", "{:.6e}".format(chi_Ps))
        print("χ_Ps AQ, rebabe pimc              ", "{:.6e}".format(chi_Ps_pimc))

        if False:
            # This is the same calculation as 'chi_Ps', but with full length equation
            chi_Ps_verif = eq_34(
                au, p=1, N=2, q=-au.e, m=au.m_e, M=au.m_e,
                r2_1N=r2_mean(a_const(au, m_r(au.m_e, au.m_e))),
                r2_12=0.0,
                r2_N1N=0.0
            )
            print("χ_Ps AQ, rebabe anl. <r^2> verif. ", "{:.6e}".format(chi_Ps_verif))
    print("Ps ----------------------------")
    print_Ps()
    print("")



def print_energy_of_H():
    """
    See Atkins, Molecular quantum mechanics 5th ed. p.88, eq. 3.44
    """
    Z = 1
    c = au, au.m_e
    E = lambda n, m, c: -((Z**2 * m * c.e**4) / (32 * np.pi**2 * c.eps0**2 * c.hbar**2)) * 1/n**2

    E_au_AQ = E(1, m_r(au.m_e, au.m_p), au)
    E_au_BO = E(1, au.m_e, au)
    E_au_Ps = E(1, au.m_e/2, au)

    print("E_H AQ au   ", E_au_AQ)
    print("E_H BO au   ", E_au_BO)
    print("E_Ps AQ au  ", E_au_Ps)


def main():
    print("All calculations assume T=0K, ground state\n")
    print("--------------------")
    susceptibility_reference_values()
    print("--------------------")
    print("Analytical energies of few systems:")
    print_energy_of_H()

main()
