import numpy as np
import h5py

import matplotlib.pyplot as plt
from PIL import Image, ImageDraw

#for file in ["001.h5", "002.h5", "003.h5", "004.h5", "005.h5", "006.h5"]:
hf_T300 = h5py.File("H2_paths_T300.h5", 'r')
hf_T3000 = h5py.File("H2_paths_T3000.h5", 'r')
T300_e1 = np.array(hf_T300.get("/e/p1/r"))
T300_e2 = np.array(hf_T300.get("/e/p2/r"))
T300_p1 = np.array(hf_T300.get("/p/p1/r"))
T300_p2 = np.array(hf_T300.get("/p/p2/r"))
T3000_e1 = np.array(hf_T3000.get("/e/p1/r"))
T3000_e2 = np.array(hf_T3000.get("/e/p2/r"))
T3000_p1 = np.array(hf_T3000.get("/p/p1/r"))
T3000_p2 = np.array(hf_T3000.get("/p/p2/r"))


def make_axes(num, figsize=(4.5, 4)):
    fig = plt.figure(figsize=figsize,  num=num)
    #fig.set_size_inches((9,8))
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.axis('equal')
    return fig, ax


def do_it_matplotlib():
    #fig1, ax1 = plt.subplots(1, 1, figsize=(9, 8), num="1")
    fig1, ax1 = make_axes(num="path_T300", figsize=(9, 8))
    for i in range(T300_e1.shape[0]-1):
        ax1.plot(T300_e1[i:i+2,1], T300_e1[i:i+2,0], lw=0.6, c="tab:blue")
        ax1.plot(T300_e2[i:i+2,1], T300_e2[i:i+2,0], lw=0.6, c="tab:green")
    ax1.plot(T300_p1[:,1], T300_p1[:,0], lw=0.6, c="tab:red")
    ax1.plot(T300_p2[:,1], T300_p2[:,0], lw=0.6, c="orangered")

    fig2, ax2 = make_axes(num="path_T3000", figsize=(9, 8))
    for i in range(T3000_e1.shape[0]-1):
        ax2.plot(T3000_e1[i:i+2,0], T3000_e1[i:i+2,2], lw=0.6, c="tab:blue")
        ax2.plot(T3000_e2[i:i+2,0], T3000_e2[i:i+2,2], lw=0.6, c="tab:green")
    ax2.plot(T3000_p1[:,0], T3000_p1[:,2], lw=0.6, c="tab:red")
    ax2.plot(T3000_p2[:,0], T3000_p2[:,2], lw=0.6, c="orangered")

    plt.show()

def plot_illustrative_picture():
    e1 = generate_randomish_bead_chain(5, (1, 0), 1)
    e2 = generate_randomish_bead_chain(1, (-1, 0), 1)
    p1 = generate_randomish_bead_chain(2, (1, 0), 0.15)
    p2 = generate_randomish_bead_chain(3, (-1, 0), 0.15)

    fig, ax = make_axes(num="path_illustration")
    ax.plot(e1[0], e1[1], c="tab:blue",  marker=".", markersize=7, label="electron")
    ax.plot(e2[0], e2[1], c="tab:green", marker=".", markersize=7, label="electron")
    ax.plot(p1[0], p1[1], c="orangered",   marker=".", markersize=7, label="proton")
    ax.plot(p2[0], p2[1], c="tab:red", marker=".", markersize=7, label="proton")

    # Make loop closed
    ax.plot(e1[0][[-1,0]], e1[1][[-1,0]], c="tab:blue")
    ax.plot(e2[0][[-1,0]], e2[1][[-1,0]], c="tab:green")
    ax.plot(p1[0][[-1,0]], p1[1][[-1,0]], c="orangered")
    ax.plot(p2[0][[-1,0]], p2[1][[-1,0]], c="tab:red")

    # "$t \\in \\{0, -i \\hbar \\beta\\}$"
    # "$t$ is $0$ or $-i \\hbar \\beta$"
    ax.plot(e1[0][0], e1[1][0], ls="", c="black", marker="s", markersize=7, label="$\\tau=0$ or $\\tau=\\beta$")
    ax.plot(e2[0][0], e2[1][0], ls="", c="black", marker="s", markersize=7)
    ax.plot(p1[0][0], p1[1][0], ls="", c="black", marker="s", markersize=7)
    ax.plot(p2[0][0], p2[1][0], ls="", c="black", marker="s", markersize=7)
    ax.legend(loc="upper right")
    ylim = ax.get_ylim()
    ax.set_ylim((ylim[0], ylim[1] + (ylim[1]-ylim[0])*0.3))



    plt.show()


def generate_randomish_bead_chain(seed:int = 0, offset=(0,0), scale=1.0):
    np.random.seed(seed)
    circ = np.linspace(0,2*np.pi,32)
    x = np.cos(circ)
    y = np.sin(circ)

    for i in range(len(circ)):
    	x[i]          += (np.random.normal())*0.1
    	y[i]          += (np.random.normal())*0.1
    	x[i:(i+2)%16] += (np.random.normal())*0.2
    	y[i:(i+2)%16] += (np.random.normal())*0.2
    	x[i:(i+4)%16] += (np.random.normal())*0.3
    	y[i:(i+4)%16] += (np.random.normal())*0.3

    x = x * scale + offset[0]
    y = y * scale + offset[1]
    return x, y

do_it_matplotlib()
plot_illustrative_picture()
