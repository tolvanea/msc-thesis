# Plot H_var_tau with confidence intervals
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import to_rgb
from miscellaneous.general_tools import linear_errorbardata_fit

def set_errobar_fake_transparency(line, caps, barlines, alpha=0.3):
    # Set errorbar color lighter
    def brighten(color, amount):
        """amount == 0.0: no change, amount == 1.0: white """
        return tuple(map(lambda c: c + (1-c)*amount, color))
    color = to_rgb(line.get_color())
    for cap in caps:
        cap.set_color(brighten(color, alpha))
    for barline in barlines:
        barline.set_color(brighten(color, alpha))

def hide_error_bar_with_fake_transparency(caps, barlines):
    for cap in caps:
        cap.set_color((1.0, 1.0, 1.0))
    for barline in barlines:
        barline.set_color((1.0, 1.0, 1.0))

def plot_main_curve(ax, x, data, errorbar):
    line, caps, barlines = ax.errorbar(
        x,
        data,
        errorbar,
        #label="${} = {}\\;${}".format("T", "3000", "K"),
        label="PIMC data",
        c="C2",
        capsize=4,
    )
    set_errobar_fake_transparency(line, caps, barlines, alpha=0.3)

    ax.set_xlabel("$\\Delta\\tau$")
    ax.set_ylabel("$-\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$", rotation='horizontal')
    ax.yaxis.set_label_coords(-0.1,1.02)

def plot_other_values(ax, x, data, errorbar):
    line, caps, barlines = ax.errorbar(
        x,
        data,
        errorbar,
        #label="${} = {}\\;${}".format("T", "3000", "K"),
        c="C2",
        ls="",
        capsize=4,
    )
    set_errobar_fake_transparency(line, caps, barlines, alpha=0.7)

def plot_reference(ax):
    line, caps, barlines = ax.errorbar(
        0.0,
        2.99071e-11,
        [0],
        label="ref. $T=0\\;$K",
        ls="",
        marker='o',
        #markersize=3,
        color=(0.35, 0.35, 0.35)
    )
    hide_error_bar_with_fake_transparency(caps, barlines)


def plot_confidense_intervals(ax, x, data, errorbar):
    plot_res = 61
    x_fit = np.linspace(0.0, x[-1]*1.2, plot_res)
    top_curve = []
    bottom_curve = []

    for k in range(plot_res):
        mean, err = linear_errorbardata_fit(
            x,
            data,
            errorbar,
            x_fit[k],
            mc_lines=1000,
            err_sigma=2
        )
        top_curve.append(mean + err)
        bottom_curve.append(mean - err)

    l = "deviation of linear fits"
    line, caps, barlines = ax.errorbar(x_fit, top_curve, x_fit*0, ls=":", c="C1", label=l)
    hide_error_bar_with_fake_transparency(caps, barlines)
    line, caps, barlines = ax.errorbar(x_fit, bottom_curve, x_fit*0, ls=":", c="C1")
    hide_error_bar_with_fake_transparency(caps, barlines)

    ax.plot(x_fit, top_curve, ls=":", c="C1")
    ax.plot(x_fit, bottom_curve, ls=":", c="C1")

    line, caps, barlines = ax.errorbar(
        [0],
        [(top_curve[0] + bottom_curve[0]) / 2],
        [(top_curve[0] - bottom_curve[0]) / 2],
        c="C0",
        ls="",
        capsize=4,
        zorder=10,
        label="estimate at $\\Delta\\tau=0$"
    )


def main():
    x = np.array([
        0.005,
        0.01,
        0.02,
        0.03,
        0.04,
        0.05,
        0.08
    ])
    data = np.array([
        2.980e-11,
        2.9761e-11,
        2.964e-11,
        2.9453e-11,
        2.932e-11,
        2.915e-11,
        2.877e-11,
    ])
    errorbar = np.array([
        0.004e-11,
        0.0023e-11,
        0.005e-11,
        0.0034e-11,
        0.006e-11,
        0.004e-11,
        0.005e-11,
    ])
    x_crop = x[[1,3,5]]
    y_crop = data[[1,3,5]]
    e_crop = errorbar[[1,3,5]]
    x_out = x[[0,2,4,6]]
    y_out = data[[0,2,4,6]]
    e_out = errorbar[[0,2,4,6]]
    fig, ax = plt.subplots(1, 1, figsize=(4.0, 3.5), num="H_vartau")
    ax.set_xlim((-0.0015, 0.06)) #0.09
    ax.set_ylim((2.90e-11, 3.01e-11)) # 2.85e-11
    plot_main_curve(ax, x_crop, y_crop, 2*e_crop)
    #plot_other_values(ax, x_out, y_out, 2*e_out)
    plot_confidense_intervals(ax, x_crop, y_crop, 2*e_crop)
    plot_reference(ax)


    ax.legend()
    fig.tight_layout()

    plt.show()

if __name__ == "__main__":
    main()
