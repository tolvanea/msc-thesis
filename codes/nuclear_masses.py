# This file plots suceptibilities with varying mass, and calculates coefficients for the fit
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import to_rgb

# If true, use fit X = a*m^-1 + b*m^-0.5 + c*m^-0.333 + X_inf
# If false, use fit X = a*m^b + X_inf
use_inverse_polynomial = False
# Least squares fit to not overweight the datapoint of dipositronium
lsqt_weighting_is_value_independent = True


# Ps2 var_M values:
# T=1000
m_T1000 = np.array([0.00163, 0.00490, 0.01470, 0.04411,
     0.125, 0.25, 0.5, 1, 2, 4, 8])
chi_T1000 = np.array([-1.434e-10, -8.15e-11, -6.283e-11, -5.604e-11, -5.315e-11,
     -5.216e-11, -5.157e-11, -5.108e-11, -5.082e-11, -5.072e-11, -5.054e-11])
chi_err_T1000 = np.array([0.006e-10, 0.04e-11  , 0.027e-11, 0.028e-11, 0.017e-11, 0.022e-11, 0.016e-11, 0.021e-11, 0.028e-11, 0.032e-11, 0.030e-11])
# BO: M=inf
chi_inf_T1000 = -4.957e-11
chi_err_inf_T1000 = 0.015e-11


# T=300
m_T300= np.array([0.00054, 0.00163, 0.00490, 0.01470, 0.04411,
     0.125, 0.25, 0.5, 1, 2, 4, 8])
chi_T300 = np.array([-4.90e-10, -1.456e-10, -8.13e-11, -6.31e-11, -5.63e-11, -5.317e-11,
                 -5.213e-11, -5.126e-11, -5.063e-11, -5.039e-11, -4.99e-11, -4.99e-11])
chi_err_T300 = np.array([0.04e-10, 0.022e-10, 0.10e-11, 0.07e-11, 0.06e-11, 0.027e-11,
                 0.033e-11, 0.032e-11, 0.023e-11, 0.030e-11, 0.04e-11, 0.04e-11])
# BO: M=inf
chi_inf_T300 = -4.965e-11
chi_err_inf_T1000 = 0.027e-11


# T=3000
m_T3000= np.array([0.125, 0.25, 0.5, 1, 2, 4, 8])
chi_T3000 = np.array([-5.401e-11, -5.302e-11, -5.249e-11,
                 -5.200e-11, -5.190e-11, -5.183e-11, -5.167e-11])
chi_err_T3000 = np.array([0.014e-11, 0.018e-11, 0.020e-11,
                 0.014e-11, 0.018e-11, 0.033e-11, 0.017e-11])
# BO: M=inf
chi_inf_T3000 = -4.969e-11
chi_err_inf_T3000 = 0.012e-11



def main():
    for name, magnified_plot in (("H2_varM", True), ("Ps2_varM", False)):
        fig, ax = plt.subplots(1, 1, figsize=(4.0, 3.8), num=name)
        plot_data_and_fit(ax, magnified_plot)
        ax.set_xlabel("$m\\;(m_\\mathrm{p})$")
        ax.set_ylabel("$-\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$", rotation='horizontal')
        ax.yaxis.set_label_coords(-0.1,1.02)
        fig.subplots_adjust(bottom=0.15, left=0.16)

    plt.show()


def errorbar_to_axis(ax, x, data, err, label, ls, color):
    line, caps, barlines = ax.errorbar(
        x,
        data,
        err if err is not None else np.zeros(len(x)),
        label=label,
        ls=ls,
        c=color,
        capsize=4,
    )
    if err is not None:
        # alpha=0.3 is the correct transparency if there is connecting line, that is 'ls!=""' !
        set_errobar_fake_transparency(line, caps, barlines, alpha=0.1)
    else:
        hide_error_bar_with_fake_transparency(caps, barlines)


def plot_references(ax, refdata):
    for ref in refdata:
        if ref[4] is not None:
            label, x, y, color, marker, size = ref
        else:  # Steal properties of other label to avoid retyping
            _, x, y, index, _ = ref
            template = refdata[index]
            label = None
            color = template[3]
            marker = template[4]
            size = template[5]
        line, caps, barlines = ax.errorbar(
            x,
            y,
            [0],
            label=label,
            ls="",
            marker=marker,
            markersize=size,
            color=color
        )
        hide_error_bar_with_fake_transparency(caps, barlines)



# Fit function for susceptibility:
#   X = a*m^-1 + b*m^-0.5 + c*m^-0.333 + X_inf
# or
#   X = a*m^b + X_inf
def plot_data_and_fit(ax, magnification=False):

    def mat(x):
        return np.vstack((
                1/x,
                1/np.sqrt(x),
                1/np.cbrt(x),
                #1/np.sqrt(np.sqrt(x)),
            )).T

    def teach_third_order(x, y):
        model = np.linalg.lstsq(mat(x), y, rcond=None)[0]
        return model

    def predict_third_order(x, model):
        y = mat(x) @ model
        return y

    def teach_exponential(x, y):
        if y[0] < 0:
            sign = -1
        else:
            sign = 1
        c1, c2 = np.polyfit(np.log(x), np.log(sign*y), 1)
        # coefficients a,b to:  'susceptibility = a*m^b + c'
        model = (sign*np.exp(c2), c1)
        return model

    def predict_exponential(x, model):
        y = model[0]*x**model[1]
        return y

    if use_inverse_polynomial: # Use fit X =a*m^-1 + b*m^-0.5 + c*m^-0.333 + X_inf
        teach = teach_third_order
        predict = predict_third_order
    else: # use fit X = a*m^b + X_inf
        teach = teach_exponential
        predict = predict_exponential


    mrange = np.geomspace(0.00054, 10000, 100)

    # I did this test-calculation separately: What if nucleus is even lighter than
    # positronium, does the susceptibility keep incresing? Answer, yes it does.
    #m2_ext = np.hstack(((0.00027,), m_T300))  # Append this new calculation to the datas.
    #chi2_ext = np.hstack(((-1.62e-9,), chi_T300))
    #err2_ext = np.hstack(((0.02e-9,), chi_err_T300))


    # Btw, fitting is best to T=1000K data. Fits at temperatures 300K and 3000K are crap
    plots = [
        ("300", m_T300, chi_T300, chi_err_T300, chi_inf_T300, "C0"),
        ("1000", m_T1000, chi_T1000, chi_err_T1000, chi_inf_T1000, "C1"),
    ]
    if magnification:
        plots.append(("3000", m_T3000, chi_T3000, chi_err_T3000, chi_inf_T3000, "C2"))

    for T, m, chi, err, chiinf, color in plots:
        print("\nT=", T, "K")

        # Datapoint weighting in curve fitting. Otherwise least squares makes too large
        # weight for the greatest y-value, that is, positronium nucleus.Weights are by
        # 1/chi^2, so all data points have about same weight, even though one is 10 times
        # larger than other
        if lsqt_weighting_is_value_independent:
            idxs = []
            for i, weight in enumerate(1/chi**2 * 1.01*chi[0]**2):
                idxs.extend([i]*int(weight))
            x_train, y_train = m[idxs], chi[idxs]
        else:
            x_train, y_train = m, chi

        # Plot data
        label_data="${} = {}\\;${}".format("T", T, "K")
        errorbar_to_axis(ax, m, -chi, err, label_data, "", color)

        print_for_each_line = lambda prefix, l1, l2: ", ".join(
            ["\n    {}{}: {:.3e}".format(prefix,x[0], x[1]) for x in zip(l1, l2)]
        )

        # print fit difference
        res = teach(x_train, y_train - chiinf)
        print("Coefficients:", print_for_each_line("", ["a", "b", "c", "d"], res))
        fit = predict(m, res) + chiinf
        print("Relative accuracy:", print_for_each_line("m=", m, (chi - fit) / chi))

        # plot continous fit
        fit_continuous = predict(mrange, teach(x_train, y_train - chiinf)) + chiinf
        label_fit = "fit $T = {}\\;$K".format(T)
        errorbar_to_axis(ax, mrange, -fit_continuous, None, label_fit, ":", color)

    # Reference data
    # label, x, y, color, marker, size
    refdata = [
        ("ref. H$_2$ $0\\;$K", 1.0,  5.20440e-11,  "grey", "o",              5),
        ("ref. BO $0\\;$K",    10.0, 4.952e-11,    "grey", "$\\rightarrow$", 15),
        ("ref. D$_2$ $0\\;$K", 2.0,  5.16429e-11,  "grey", "^",              None),
    ]
    if magnification:
        refdata.extend([
            ("ref. H$_2$ $0\\;$K", 1.0,  5.2065e-11,  0, None),
            ("asdf H2",            1.0,  5.1131e-11,  0, None),
            ("asdf H2",            1.0,  5.08e-11,    0, None),
            ("asdf H2",            1.0,  5.01e-11,    0, None),
            ("asdf H2",            1.0,  5.04e-11,    0, None),
            ("asdf D2",            2.0,  5.0650e-11,  2, None),
            ("asdf BO",            10.0, 5.0530e-11,  1, None),
            ("asdf BO",            10.0, 4.789e-11,   1, None),
            ("asdf BO",            10.0, 5.0699e-11,  1, None),
            ("asdf BO",            10.0, 5.037e-11,  1, None),
            ("asdf BO",            10.0, 5.201e-11,  1, None),
        ])
        ax.set_xlim(0.1, 15)
        #ax.set_ylim(4.30e-11, 5.37e-11)
        ax.set_ylim(4.78e-11, 5.95e-11)
    else:
        refdata.append(
            ("ref. Ps$_2$ $0\\;$K",   0.00054, 6.974490e-10, "grey", "*",  None),
        )
        ax.set_yscale('log')
        ax.set_xlim(0.0003, 15)
        ax.set_ylim(4.35e-11, 8e-10)

    plot_references(ax, refdata)
    ax.set_xscale('log')
    ax.legend()


def set_errobar_fake_transparency(line, caps, barlines, alpha=0.3):
    # Set errorbar color lighter
    def brighten(color, amount):
        """amount == 0.0: no change, amount == 1.0: white """
        return tuple(map(lambda c: c + (1-c)*amount, color))
    color = to_rgb(line.get_color())
    for cap in caps:
        cap.set_color(brighten(color, alpha))
    for barline in barlines:
        barline.set_color(brighten(color, alpha))

def hide_error_bar_with_fake_transparency(caps, barlines):
    for cap in caps:
        cap.set_color((1.0, 1.0, 1.0))
    for barline in barlines:
        barline.set_color((1.0, 1.0, 1.0))


if __name__ == "__main__":
    main()
