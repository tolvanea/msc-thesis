# Plot susceptibility figures to poster. Combines two y-axes
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import to_rgb
from miscellaneous.general_tools import linear_errorbardata_fit

def set_errobar_fake_transparency(line, caps, barlines, alpha=0.3):
    # Set errorbar color lighter
    def brighten(color, amount):
        """amount == 0.0: no change, amount == 1.0: white """
        return tuple(map(lambda c: c + (1-c)*amount, color))
    color = to_rgb(line.get_color())
    for cap in caps:
        cap.set_color(brighten(color, alpha))
    for barline in barlines:
        barline.set_color(brighten(color, alpha))

def hide_error_bar_with_fake_transparency(caps, barlines):
    for cap in caps:
        cap.set_color((1.0, 1.0, 1.0))
    for barline in barlines:
        barline.set_color((1.0, 1.0, 1.0))

def plot_reference_from_configure_instance(ax, refdata):
    for ref in refdata:
        if ref[3] is None:
            label = ref[0]
            color = ref[4]
        else:
            label = None
            color = refdata[ref[3]][4]
        line, caps, barlines = ax.errorbar(
            ref[1],
            ref[2],
            [0],
            label=label,
            ls="",
            marker='o',
            #markersize=3,
            color=color
        )
        hide_error_bar_with_fake_transparency(caps, barlines)


# What is this for? Some hack I suppose? Damnit me.
def plot_grey_dot(ax):
    line, caps, barlines = ax.errorbar(
        0.0,
        0.0,
        [0],
        label="ref.",
        ls="",
        marker='o',
        #markersize=3,
        color="grey"
    )
    hide_error_bar_with_fake_transparency(caps, barlines)

def plot_split_curves(
    x1, y1, e1, l1, ylim1, ls1, c1,
    x2, y2, e2, l2, ylim2, ls2, c2,
    xlim, figax, more_plots_coming=False, plot_it=((True,True), (True,True)), ax_ratio=1/2
):
    fig, (ax2, ax1) = figax
    for i, ax in enumerate([ax2, ax1]):
        for lab, c, ls, (x, y, e), j in [
            (l1, c1, ls1, (x1, y1, e1), 0),
            (l2, c2, ls2, (x2, y2, e2), 1)
        ]:
            if not plot_it[i][j]:
                continue
            line, caps, barlines = ax.errorbar(
                x, y, e,
                label=lab,
                ls=ls,
                c=c,
                capsize=4,
            )
            set_errobar_fake_transparency(line, caps, barlines, alpha=0.3)
    if more_plots_coming:
        return

    sep = 0.13
    fig.subplots_adjust(hspace=sep)  # adjust space between axes
    ax2.set_ylim(ylim2)
    ax1.set_ylim(ylim1)
    ax2.set_xlim(xlim)
    ax1.set_xlim(xlim)

    ax2.spines['bottom'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax2.xaxis.tick_top()
    ax2.tick_params(axis='x', which='both',length=0)
    ax2.tick_params(labeltop=False)  # don't put tick labels at the top
    ax1.get_yaxis().get_offset_text().set_visible(False)
    ax1.xaxis.tick_bottom()

    M = 21
    rip_length = 0.08
    rip_x = np.linspace(-1/42, 1 + 1/(2*(M-1)), M)
    rip_y = np.ones(len(rip_x)) - rip_length/2 * (0.5/ax_ratio)
    rip_y[np.arange(M)%2==1] += rip_length* (0.5/ax_ratio)
    #rip_border_x = np.array([-1/42, 1/42])
    #rip_border_y = np.array([1, 1.1])

    #ax2.plot(rip_x, rip_y+0.1, transform=ax1.transAxes, color='k', lw=0.5, mec='k', mew=1, clip_on=False)
    for py in [0.0, sep * 0.3 / ax_ratio]:
        ax1.plot(rip_x, rip_y+py, transform=ax1.transAxes, color='k', lw=0.7, mec='k', mew=1, clip_on=False)
        #for px in [0,1]:
            #ax1.plot(rip_border_x+px, rip_border_y+py, transform=ax1.transAxes, color='k', lw=2, mec='k', mew=1, clip_on=False)

    ax1.set_xlabel("$T$ $(\\mathrm{{K}})$")
    ax2.set_ylabel("$-\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$", rotation='horizontal')
    ax2.yaxis.set_label_coords(-0.1,1.02)

def plot_Ps_Ps2():
    # Ps
    #x1 = np.array([300, 1000, 3000, 5000,])
    #e1 = np.array([0.004e-10, 0.0027e-10, 0.0025e-10, 0.0030e-10,])
    x1 = np.array([100, 300, 1000, 3000, 5000,])
    y1 = np.array([2.377e-10, 2.3654e-10, 2.3175e-10, 2.1822e-10, 2.0477e-10,])
    e1 = np.array([0.006e-10, 0.0023e-10, 0.0015e-10, 0.0014e-10, 0.0014e-10,])
    # Ps2
    x2 = np.array([100, 300, 500,])
    y2 = np.array([4.929e-10, 4.928e-10, 4.914e-10,])
    e2 = np.array([0.025e-10, 0.015e-10, 0.014e-10,])

    #fig = plt.figure(figsize=(3.5, 3.0), num="Ps_Ps2") # used in poster
    #fig = plt.figure(figsize=(3.5, 3.5), num="Ps_Ps2_taller") # "taller", used in slides
    fig = plt.figure(figsize=(4.0, 3.5), num="Ps_Ps2_mscthesis") # larger, used in diploma thesis
    ax2 = plt.subplot2grid((3, 1), (0, 0), colspan=1, rowspan=1)
    ax1 = plt.subplot2grid((3, 1), (1, 0), colspan=1, rowspan=2)
    figax = fig, (ax2, ax1)


    plot_split_curves(
        x2, y2, e2, "Ps$_2$ AQ", (2.235e-10, 2.415e-10), "-", "C0",
        x1, y1, e1, "Ps AQ", (4.885e-10, 4.975e-10), "-", "C2",
        (-100, 2000), figax, ax_ratio=2/3
    )

    refdata_Ps = (
        (     None,            0.0, 2.388663e-10, None, "C2"),
        #(     "ref. Ps AQ",   0.0, 2.388663e-10, None, "C2"),
    )

    plot_reference_from_configure_instance(ax1, refdata_Ps)
    plot_reference_from_configure_instance(ax2, refdata_Ps)
    plot_grey_dot(ax1)
    ax1.legend()
    #fig.tight_layout()
    fig.subplots_adjust(bottom=0.15, left=0.16)

def plot_H_H2():

    # H (AQ)
    x11 = np.array([300, 1000, 3000,])
    y11 = np.array([2.989e-11, 2.992e-11, 2.9915e-11,])
    e11 = np.array([0.005e-11, 0.004e-11, 0.0032e-11,])
    # H (BO)
    x12 = np.array([300, 1000, 3000,])
    y12 = np.array([2.9870e-11, 2.9841e-11, 2.983e-11,])
    e12 = np.array([0.0031e-11, 0.0022e-11, 0.004e-11,])

    # H2 (AQ)
    x21 = np.array([300, 1000, 3000,])
    y21 = np.array([5.063e-11, 5.075e-11, 5.200e-11,])
    e21 = np.array([0.024e-11, 0.019e-11, 0.013e-11,])
    # H2 (BO)
    x22 = np.array([300, 1000, 3000,])
    y22 = np.array([4.965e-11, 4.957e-11, 4.969e-11,])
    e22 = np.array([0.027e-11, 0.015e-11, 0.012e-11,])

    fig = plt.figure(figsize=(3.5, 3.5), num="H_H2")
    ax2 = plt.subplot2grid((3, 1), (0, 0), colspan=1, rowspan=2)
    ax1 = plt.subplot2grid((3, 1), (2, 0), colspan=1, rowspan=1)
    figax = fig, (ax2, ax1)

    plot_split_curves(
        x21, y21, e21, "H$_2$ AQ", None, "-", "C0",
        x22, y22, e22, "H$_2$ BO", None, "--", "C1",
        None, figax, more_plots_coming=True, ax_ratio=None,
    )
    plot_split_curves(
        x11, y11, e11, "H AQ", (2.976e-11, 2.999e-11), "-", "C2",
        x12, y12, e12, "H BO", (4.901e-11, 5.22e-11), "--", "C3",
        (-100, 3100), figax, ax_ratio=1/3,
    )

    refdata_H = (
        (     None,   0.0, 2.9907e-11, None, "C2"), # "ref. H AQ"
        (     None,   0.0, 2.9858e-11, None, "C3"), # "ref. H BO"

    )
    refdata_H2 = (
        (     None,   0.0, 5.2065e-11, None, "C0"), #"ref. H$_2$ AQ"
        (     None,   0.0, 5.0699e-11, None, "C1"), #"ref. H$_2$ BO"
        (     None,   0.0,  4.815e-11,    1, None),
        (     None,   0.0,  4.952e-11,    1, None),
        (     None, 300.0,  5.080e-11,    0, None),
        (     None,   0.0, 5.0530e-11,    1, None),
        (     None,   0.0, 5.1131e-11,    0, None),
        (     None,   0.0,   5.08e-11,    0, None),
        (     None,   0.0,   5.01e-11,    0, None),
        (     None,   0.0,   5.04e-11,    0, None),
    )
    plot_reference_from_configure_instance(ax1, refdata_H)
    plot_reference_from_configure_instance(ax2, refdata_H2)
    #plot_reference_H(ax1)
    #plot_reference_H2(ax2)
    plot_grey_dot(ax2)
    #ax1.legend(loc="center right")
    ax2.legend(loc="lower right")
    #figax[0].tight_layout()
    fig.subplots_adjust(bottom=0.15, left=0.16)


def plot_He():

    # He AQ
    x11 = np.array([300, 1000, 3000,])
    y11 = np.array([2.379e-11, 2.369e-11, 2.375e-11,])
    e11 = np.array([0.016e-11, 0.013e-11, 0.007e-11,])
    # He BO
    x12 = np.array([300, 1000, 3000,])
    y12 = np.array([2.376e-11, 2.373e-11, 2.371e-11,])
    e12 = np.array([0.010e-11, 0.009e-11, 0.004e-11,])

    fig, ax = plt.subplots(1, 1, figsize=(3.5, 3), num="He")
    line, caps, barlines = ax.errorbar(
        x11, y11, e11,
        label="He AQ",
        ls="-",
        c="C0",
        capsize=4,
    )
    set_errobar_fake_transparency(line, caps, barlines, alpha=0.3)
    line, caps, barlines = ax.errorbar(
        x12, y12, e12,
        label="He BO",
        ls="--",
        c="C1",
        capsize=4,
    )
    set_errobar_fake_transparency(line, caps, barlines, alpha=0.3)

    refdata_He = (
        (     None,           0.0, 2.36e-11,    None, "C0"),  # "ref. He AQ"
        (     None,           0.0, 2.37569e-11, None, "C1"),  # "ref. He BO"
        (     None,           0.0, 2.37600e-11,    0, None),
        (     None,           0.0, 2.40e-11,       0, None),
        (     None,           0.0, 2.375e-11,      1, None),
    )
    plot_reference_from_configure_instance(ax, refdata_He)
    plot_grey_dot(ax)
    #print(ax.get_ylim())
    ax.set_ylim((2.3538e-11, 2.4022e-11))
    ax.legend()

    ax.set_xlabel("$T$ $(\\mathrm{{K}})$")
    ax.set_ylabel("$-\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$", rotation='horizontal')
    ax.yaxis.set_label_coords(-0.1,1.02)
    fig.subplots_adjust(bottom=0.15, left=0.16)


def main():

    plot_Ps_Ps2()
    plot_H_H2()
    plot_He()

    plt.show()

main()
