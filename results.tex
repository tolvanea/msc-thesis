\chapter{Diamagnetic susceptibilities of light atoms and molecules}
\label{ch:results}

Diamagnetic susceptibilities are studied with PIMC simulations, which are carried out on systems listed in table~\ref{tab:systems}. These systems contain light nuclei, which undergo significant nonadiabatic effects. Diatomic molecules, such as $\mathrm{H}_2$, have rotational and vibrational states that are activated by the finite temperature. The isotope effects of the diatomic molecules are inspected by replacing the protons $(m_\mathrm{p})$ by variable nuclear masses including positrons $(m_\mathrm{e})$, deuterons $(2 m_\mathrm{p})$, and artificially weighted nuclei ($m_\mathrm{e}$ -- $8 m_\mathrm{p}$). The positronic systems represent special interest, because they show the extreme limit of the nonadiabatic effects, which is usually challenging to simulate. The positronic systems are unstable, but they are still relevant in some spectroscopic arrangements.

\newcommand{\twoliner}[2]{
    \hspace{-0.2cm}\begin{tabular}{l}
        #1 \\[-0.15cm]
        #2 \\
    \end{tabular}\hspace{-0.4cm}
}
\begin{table}[!tb]
    \centering
    \caption{List of simulated systems. A positronium is an exotic one-electron system that has a positron in place of its nucleus.}
    \label{tab:systems}
    \begin{tabular}{lllccc}
        \textbf{Symbol} & \textbf{Nucleus} & \textbf{Name} & \textbf{Table} $E$& \textbf{Table} $\chi$ & \textbf{Figure} $\chi$\\
        \hline
        $\mathrm{H}$     &AQ/BO &hydrogen atom                 &\ref{tab:energia1} &\ref{tab:susc1} &\ref{fig:tauexp},~\ref{fig:susc1a} \\
        $^4\mathrm{He}$    &AQ/BO &helium atom                   &\ref{tab:energia1} &\ref{tab:susc1} &\ref{fig:susc1b} \\
        $\mathrm{H}_2$   &AQ/BO &hydrogen molecule             &\ref{tab:energia1} &\ref{tab:susc2} &\ref{fig:susc12a},~\ref{fig:susc2} \\
        $\mathrm{HD}$    &AQ    &hydrogen deuteride            &\ref{tab:energia1} &\ref{tab:susc3} & \\
        $\mathrm{D}_2$   &AQ    &deuterium molecule            &\ref{tab:energia1} &\ref{tab:susc2} &\ref{fig:susc2}\\
        $\mathrm{H}_2^+$ &AQ    &hydrogen molecule ion         &\ref{tab:energia1} &\ref{tab:susc3} & \\
        $\mathrm{Ps}$    &AQ    &positronium atom              &\ref{tab:energia1} &\ref{tab:susc3} & \\
        $\mathrm{Ps}_2$  &AQ    &positronium molecule          &\ref{tab:energia1} &\ref{tab:susc2} &\ref{fig:susc2}\\
        $h_2$           &AQ    &\twoliner{artificial homonuclear}{diatomic molecules} &                   &                &\ref{fig:susc2}\\
        %&\ref{tab:energia1} &\ref{tab:susc2} &\ref{fig:susc2}\\
        %\multirow{2}{*}{$h_2$}           &\multirow{2}{*}{AQ}    &artificial homonuclear &                   &                &\multirow{2}{*}{\ref{fig:susc2}}\\[-2mm]
        %& &diatomic molecules & & &\\
    \end{tabular}
\end{table}

The inspected systems have no more than two particles of the same kind, so by assuming opposite spins for the identical particles, the indistinguishability does not pose a problem. Fortunately, the ground state of two electron system does have opposite spins. Also, the paramagnetic contribution is cancelled out if the two spins are opposite, and the total magnetic susceptibility is given by the diamagnetic susceptibility. All inspected systems are diamagnetic, except for $\mathrm{H}$ and $\mathrm{H}_2^+$.

Energies are measured with the virial energy estimator~\cite[p.~23]{kylanpaaFirstprinciplesFiniteTemperature2011}, and diamagnetic susceptibilities are measured with the estimator given in equation~\ref{eq:finalSuscEqOfAll}. First, the energies are compared to reference values, which shows that simulations are fairly accurate. Then, the susceptibilities are presented. Nonadiabatic \textit{All Quantum} (AQ) nuclei are compared to fixed \textit{Born--Oppenheimer} (BO) nuclei.

Appendix~\ref{ch:sorcery} links additional information of the applied simulations. It includes, \textit{e.g.}, used simulation parameters and more results from other observables. The simulations were carried out using a software developed in our research group~\cites{kylanpaaFirstprinciplesFiniteTemperature2011}{, tiihonenThermalEffectsAtomic2019}.

\section{Unit conversions of the susceptibility}
It should be emphasized that this work utilizes the SI convention of magnetism instead of the widely utilized Gaussian convention. The magnetic susceptibility in SI convention is $4\pi$ times larger than in Gaussian convention. Also, it should be noted that where previous sections considered susceptibility system-wise, this section presents susceptibilities per mole of gas. SI molar susceptibility has unit $[\frac{\text{m}^3}{\text{mol}}]$, and it can be converted from Gaussian cgs unit $[\frac{\text{cm}^3}{\text{mol}}]$ with
\begin{align}
    \chi_{\text{mol}}^{\text{SI}}
    &= 4 \pi \cdot \num{e-6}\; \chi_{\text{mol}}^{\text{cgs\;(Gaussian)}}\\
    &\approx \num{1.25664e-05}\; \chi_{\text{mol}}^{\text{cgs\;(Gaussian)}}.
\end{align}
Similarly, the conversion factors for the atomic units $[a_0^3]$ are
\begin{align}
    \chi_{\text{mol}}^{\text{SI}}
    &\approx \num{8.92389e-08}\; \chi^{\text{a.u.\;(SI)}}\\
    &\approx \num{1.12141e-06}\; \chi^{\text{a.u.\;(Gaussian)}}.
\end{align}

In this section the susceptibility $\chi_{\text{mol}}^{\text{SI}}$ is denoted with $\chi$.



\section{Time step length extrapolation}
The accuracy of PIMC simulations depends on the time step length parameter $\Delta\tau$. The shorter values of $\Delta\tau$ pose smaller discretization error, while they make calculations computationally heavier. Extrapolation to $\Delta\tau \to 0$ may be required to obtain non-biased estimates. All results presented in this work reflect the limit $\Delta\tau = 0$ estimate.

The time step length is extrapolated at $\Delta\tau=0$ with a linear fit, which is based on calculations using $\Delta\tau \in \{0.01,\,0.03,\,0.05\}$, where the unit of $\Delta\tau$ is an inverse of Hartree energy $\si{\hartree}^{-1}$. Systems $\mathrm{Ps}$ and $\mathrm{Ps}_2$ are exceptions, which are calculated with $\Delta\tau \in \{0.1,\,0.3,\,0.5\}$. The longer time steps can be used for $\mathrm{Ps}$ and $\mathrm{Ps}_2$, because light nuclei have smaller errors due to the finite time step, which allows the use of the longer time step for increased statistical accuracy.

The susceptibility estimator~\ref{eq:finalSuscEqOfAll} has a strong linear discretization error, which is demonstrated in figure~\ref{fig:tauexp}. %
\begin{figure}[!tb]
    \centering
    \includegraphics[width=0.50\textwidth]{figures/H_vartau.pdf}
    \caption{Demonstration of the linear extrapolation, which is used to estimate the observable and the error bar in the limit of $\Delta\tau=0$. The observable in this case is the susceptibility, which has a virtually linear error with $\Delta\tau$. The system is $H$ (AQ) and the temperature is $T=\SI{3000}{\kelvin}$. The extrapolation to $\Delta\tau=0$ matches the reference value, which is given in table~\ref{tab:susc1}.}
    \label{fig:tauexp}
\end{figure}%
The figure also shows linear extrapolation, which is applied for all susceptibility and energy values. In most cases, the energy values are so accurate that they are not significantly affected by the extrapolation. The energy of $\mathrm{Ps}_2$ shows nonlinearity within utilized time step values, and as an exception, it is presented with $\Delta\tau = 0.1$ calculation instead of extrapolation.

The statistical uncertainty at $\Delta\tau=0$ is estimated by resampling the linear extrapolation based on the uncertainties of the data at each time step. First, the margins of error with a finite $\Delta\tau$ calculations are used to generate random samples. A linear regression is applied to each sample individually, and the limit $\Delta\tau=0$ is extrapolated. A deviation of extrapolations is used to set margins of error for the $\Delta\tau=0$ estimate. All margins of error are expressed with $2\;$SEM, which corresponds to 95\% confidence.

\section{Total energy}

\newcommand{\tabfootnote}[2]{
    \multicolumn{6}{l}{\rule{0pt}{1.0\normalbaselineskip} $\;^\text{#1}$\footnotesize #2}
}
\newcommand{\suscUnit}[0]{
    $-10^{-11}\,\frac{\text{m}^3}{\text{mol}}$
}


% Note that zero point energy for H$_2$ is $0.0074\;E_\text{H}$~\cite{kylanpaaHydrogenMoleculeIon2007a} and for H$_3^+$ it is $0.0202\;E_\text{H}$~\cite{kylanpaaFiniteTemperatureQuantum2010a}.

% TODO Table 1
\begin{table}[!tb]
    \centering
    \caption{PIMC energy and reference data. All PIMC calculations are in good agreement with the reference data. Energies are in the Hartree atomic units. The footnotes are:~$^\text{a}$~--~analytical result~\cite[p.~88]{atkinsMolecularQuantumMechanics2011},~$^\text{b}$~--~the system is dissociated at this temperature.}
    \label{tab:energia1}
    \begin{tabular}{ll|llll}
        $E (\si{\hartree})$
            & $T$
                & $\mathrm{H}$ (AQ)
                    & $\mathrm{H}$ (BO)
                        & $^4\mathrm{He}$ (AQ)
                            & $\mathrm{He}$ (BO)
        \\
        \hline
        \multirow{3}{*}{PIMC}
            & $\SI{300}{\kelvin}$
                & $-0.49975(12)$
                    & $-0.49996(8)$
                        & $-2.9027(14)$
                            & $-2.9030(9)$
        \\
            & $\SI{1000}{\kelvin}$
                & $-0.49979(13)$
                    & $-0.49995(8)$
                        & $-2.9020(20)$
                            & $-2.9040(15)$
        \\
            & $\SI{3000}{\kelvin}$
                & $-0.49976(16)$
                    & $-0.50008(15)$
                        & $-2.9019(21)$
                            & $-2.9023(9)$
        \\
        \cline{1-6}
        Ref.
            & $\SI{0}{\kelvin}$
                & $-0.49973$ $^\text{a}$
                    & $-0.5$ $^\text{a}$
                        & $-2.9033$~\cite{stankeRelativisticCorrectionsNonBornOppenheimer2007}
                            & $-2.9037$~\cite{drakeHighPrecisionTheory1999}
        \\
        \\

        $E (\si{\hartree})$
            & $T$
                & $\mathrm{H}_2$
                    & $\mathrm{HD}$
                        & $\mathrm{D}_2$
                            & $\mathrm{H}_2$ (BO$_{r = \SI{1.4}{\bohr}}$)
        \\
        \hline
        \multirow{3}{*}{PIMC}
            & $\SI{300}{\kelvin}$
                & $-1.1641(6)$
                    & $-1.1646(12)$
                        & $-1.1660(6)$
                            & $-1.1753(24)$
        \\
            & $\SI{1000}{\kelvin}$
                & $-1.1618(8)$
                    & $-1.1624(13)$
                        & $-1.1633(8)$
                            & $-1.1763(25)$
        \\
            & $\SI{3000}{\kelvin}$
                & $-1.1508(8)$
                    & $-1.1520(16)$
                        & $-1.1526(9)$
                            & $-1.1755(28)$
        \\
        \cline{1-6}
        Ref.
            & $\SI{0}{\kelvin}$
                & $-1.1640$~\cite{bubinVariationalCalculationsExcited2003}
                    & $-1.1655$~\cite{alexanderFullyNonadiabaticProperties2008}
                        & $-1.1672$~\cites{bubinAccurateNonBornOppenheimer2010}{alexanderFullyNonadiabaticProperties2008}
                            & $-1.1745$~\cite{wolniewiczNonadiabaticEnergiesGround1995}
        \\
        \\

        $E (\si{\hartree})$
            & $T$
                & $\mathrm{Ps}$
                    & $\mathrm{Ps}_2$
                        & $\mathrm{H}_2^+$
        \\
        \cline{1-5}
        \multirow{5}{*}{PIMC}
            & \SI{100}{\kelvin}
                & $-0.249977(21)$
                    & $-0.51589(15)$
                        & \\
            & \SI{300}{\kelvin}
                & $-0.24997(4)$
                    & $-0.51594(15)$
                        & $-0.5971(10)$ \\
            & \SI{500}{\kelvin}
                &
                    & $-0.51595(16)$
                        & \\
            & \SI{1000}{\kelvin}
                & $-0.24999(4)$
                    & ~$^\text{b}$
                        & $-0.5943(14)$ \\
            & \SI{3000}{\kelvin}
                & $-0.24997(4)$
                    &
                        & ~$^\text{b}$ \\
        \cline{1-5}
        Ref. %\multirow{1}{*}{Ref.}
            & \SI{0}{\kelvin}
                & $-0.25$ $^\text{a}$
                    & $-0.51600$~\cite{bubinRelativisticCorrectionsGroundstate2007}
                        & $-0.5971$~\cite{hijikataSolvingNonBornOppenheimer2009}
        \\
    \end{tabular}
\end{table}

Total energies of all systems are presented in table~\ref{tab:energia1}. The energy is a common benchmark quantity, and there is accurate reference data available that can be compared with the PIMC results. Finite temperature PIMC energies can be extrapolated to zero kelvin, where they can be compared to zero kelvin reference values~\cite{stankeRelativisticCorrectionsNonBornOppenheimer2007,bubinVariationalCalculationsExcited2003, bubinAccurateNonBornOppenheimer2010, drakeHighPrecisionTheory1999, alexanderFullyNonadiabaticProperties2008, wolniewiczNonadiabaticEnergiesGround1995, bubinRelativisticCorrectionsGroundstate2007, hijikataSolvingNonBornOppenheimer2009} from other high-accuracy methods. The PIMC results fit accurately to the reference values, which gives an affirmation to accuracy of the simulations.

Systems $\mathrm{H}$, $\mathrm{He}$ and $\mathrm{H}_2$ (BO) show no temperature dependence, as these systems have no nucleus--nucleus interaction. Low temperatures mostly affects nuclear bonds but not the electronic structure. The energies of the diatomic AQ systems $\mathrm{H}_2$, $\mathrm{HD}$, $\mathrm{D}_2$ and $\mathrm{H}_2^+$ increase with the temperature, because rotational and vibrational states of the nuclei are activated.

The dipositronium cannot be simulated at temperatures of \SI{1000}{\kelvin} or higher, because its ground state $\SI{-0.516}{\hartree}$ is near the dissociated state $2 E_\mathrm{Ps} = \SI{-0.50}{\hartree}$. Dipositronium is calculated at additional temperatures \SI{100}{\kelvin} and \SI{500}{\kelvin}, so that temperature dependence can be inspected. The energy of dipositronium is not temperature dependent, unlike other diatomic AQ molecules.


%\FloatBarrier
\section{Diamagnetic susceptibility}
In this section we present the diamagnetic suspectibilities based on PIMC simulations, and compare the results to selected 0 kelvin references values. The references values~\cite{rebaneNonadiabaticTheoryDiamagnetic2002, haftelPreciseNonvariationalCalculations1988, mendelsohnHartreeFockDiamagneticSusceptibilities1970b, willsMagneticSusceptibilityOxygen1924, bruchDiamagnetismHelium2000, havensMagneticSusceptibilitiesCommon1933, barterDiamagneticSusceptibilitiesSimple1960, alexanderFullyNonadiabaticProperties2008, bubinAccurateNonBornOppenheimer2010, alexanderRovibrationallyAveragedProperties2007, alijahHydrogenMoleculeH22019, glickDiamagneticSusceptibilityGases1961, ishiguroMagneticPropertiesHydrogen1954, raynesNoteMagneticSusceptiblllty1974, jainDiamagneticSusceptibilityAnisotropy2007, ruudFullCICalculations1996, bubinRelativisticCorrectionsGroundstate2007}  are based on various methods and do not fully agree with each other or the results from PIMC. Some systems do not have a reference value for the diamagnetic suspectibility, to the best of our knowledge. %The low accuracy of reference values makes validation of PIMC results difficult, and it shifts the role of PIMC results from benchmarking into bringing up new data.

A method developed by Rebane~\cite{rebaneNonadiabaticTheoryDiamagnetic2002} is used to calculate some reference values. This method can be used to estimate the diamagnetic susceptibilities of AQ/BO systems that are charge neutral and contain only two kinds of particles. The method is based on the mean distances between pairs of particles, which are easily obtained from the literature. However, the method does not seem to be accurate for nonadiabatic many-nuclei systems. Despite uncertainty of the accuracy, calculated values of this method are still included to the set of references values. % $\ev{r_{ij}^2}$ TODO korjaa muuallekkin tekstiin <r^2> pois


\subsection{Monatomic systems}

The susceptibilities of monatomic systems $\mathrm{H}$ and $\mathrm{He}$ are presented in table~\ref{tab:susc1} and plotted in figure~\ref{fig:susc1}. The hydrogen atom shows a clear distinction between AQ and BO calculations, because the adiabatic coupling has a notable effect on the susceptibility. The effect cannot be distinguished from the helium atom, because its nucleus is much heavier, which is closer to the BO nucleus. Both the hydrogen and the helium are invariant to the temperature, because the single nucleus does not have any rotational or vibrational states.

% TODO table 4
\begin{table}[!tb]
    \centering
    \caption{Diamagnetic susceptibilities of monatomic systems from PIMC and selected references~\cite{rebaneNonadiabaticTheoryDiamagnetic2002, haftelPreciseNonvariationalCalculations1988, mendelsohnHartreeFockDiamagneticSusceptibilities1970b, willsMagneticSusceptibilityOxygen1924, havensMagneticSusceptibilitiesCommon1933, bruchDiamagnetismHelium2000, barterDiamagneticSusceptibilitiesSimple1960}. The susceptibilities are in units of \suscUnit.}
    \label{tab:susc1}
    \begin{tabular}{ll|llll}
        $\chi$
            & $T$
                & $\mathrm{H}$ (AQ)
                    & $\mathrm{H}$ (BO)
                        & $^4\mathrm{He}$ (AQ)
                            & $\mathrm{He}$ (BO)
        \\
        \hline
        \multirow{3}{*}{PIMC}
            & $\SI{300}{\kelvin}$
                & $2.989(5)$
                    & $2.9861(11)$
                        & $2.376(10)$
                            & $2.376(10)$
        \\
            & $\SI{1000}{\kelvin}$
                & $2.992(4)$
                    & $2.9858(11)$
                        & $2.369(17)$
                            & $2.373(9)$
        \\
            & $\SI{3000}{\kelvin}$
                & $2.9915(32)$
                    & $2.9846(21)$
                        & $2.375(8)$
                            & $2.371(4)$
        \\
        \cline{1-6}
        \multirow{4}{*}{Ref.}
            & \multirow{1}{*}{$\SI{0}{\kelvin}$~$\;^\text{a}$}
                & $2.99071$ $^\text{b}$
                    & $2.98583$ $^\text{b}$
                        & $2.37600$~\cite{rebaneNonadiabaticTheoryDiamagnetic2002}
                            & $2.37569$~\cite{haftelPreciseNonvariationalCalculations1988}
        \\
        \cline{2-2}
            & \multirow{2}{*}{$\SI{0}{\kelvin}$}
                &
                    & $2.98577$~\cite{mendelsohnHartreeFockDiamagneticSusceptibilities1970b}
                        & $2.36$ $^\text{c}$~\cite{willsMagneticSusceptibilityOxygen1924}
                            & $2.375$~\cite{bruchDiamagnetismHelium2000}
        \\
            & % empty
                &
                    &
                        & $2.40$ $^\text{c}$~\cite{havensMagneticSusceptibilitiesCommon1933}
                            &
        \\
            & % empty
                &
                    &
                        & $2.54(10)$ $^\text{c}$~\cite{barterDiamagneticSusceptibilitiesSimple1960}
                            &
        \\
        \tabfootnote{a}{Calculated with Rebane's method. Pair distance data is taken from indicated source.}\\
        \tabfootnote{b}{Pair distance for Rebane's method is taken from analytical equation~\ref{eq:r2integral}.}\\
        \tabfootnote{c}{Experimental result, $T = \SI{298}{\kelvin}$.}\\
    \end{tabular}
\end{table}

\begin{figure}[!tb]
     \makebox[\textwidth][c]{
        \centering
        \begin{subfigure}{.50\textwidth}
            \centering
            \includegraphics[width=1.0\textwidth]{figures/H.pdf}
            \vspace*{-0.9cm}\caption{$\mathrm{H}$}
            \label{fig:susc1a}
        \end{subfigure}%
        %\hfill
        \begin{subfigure}{.50\textwidth}
            \centering
            \includegraphics[width=1.0\textwidth]{figures/He.pdf}
            \vspace*{-0.9cm}\caption{$^4\mathrm{He}$}
            \label{fig:susc1b}
        \end{subfigure}
        $\qquad$$\quad$
     }
    \caption{Diamagnetic susceptibility of $\mathrm{H}$ and $\mathrm{He}$ as a function of temperature. The PIMC values at each temperature are statistically indistinguishable, and no temperature dependence is observed nor expected for these systems. Solid lines are drawn to guide the eye. The reference values are from table~\ref{tab:susc1}.}
    \label{fig:susc1}
\end{figure}

%\FloatBarrier
\subsection{Diatomic systems}

Susceptibilities of diatomic systems $\mathrm{H}_2$, $\mathrm{HD}$, $\mathrm{D}_2$ and $\mathrm{H}_2^+$ are presented in tables~\ref{tab:susc2} and~\ref{tab:susc3}. %
% TODO table 5
\begin{table}[!tb]
    \centering
    \caption{Diamagnetic susceptibilities of diatomic systems. The susceptibilities are in units of \suscUnit.}
    \label{tab:susc2}
    \begin{tabular}{ll|llll}
        $\chi$
            & $T$
                & $\mathrm{H}_2$
                    & $\mathrm{HD}$
                        & $\mathrm{D}_2$
                            & $\mathrm{H}_2$ (BO$_{r = \SI{1.4}{\bohr}}$)
        \\
        \hline
        \multirow{3}{*}{PIMC}
            & $\SI{300}{\kelvin}$
                & $5.063(24)$
                    & $5.05(5)$
                        & $5.039(28)$
                            & $4.965(27)$
        \\
            & $\SI{1000}{\kelvin}$
                & $5.075(19)$
                    & $5.084(34)$
                        & $5.082(27)$
                            & $4.957(15)$
        \\
            & $\SI{3000}{\kelvin}$
                & $5.200(13)$
                    & $5.174(28)$
                        & $5.196(29)$
                            & $4.969(12)$
        \\
        \cline{1-6}
        \multirow{6}{*}{Ref.}
            & \multirow{1}{*}{$\SI{0}{\kelvin}$~$\;^\text{a}$}
                & $5.2065$~\cite{alexanderFullyNonadiabaticProperties2008}
                    &
                        & $5.16429$~\cite{bubinAccurateNonBornOppenheimer2010}
                            & $5.0699$~\cite{alexanderRovibrationallyAveragedProperties2007}

        \\
        \cline{2-2}
            & \multirow{4}{*}{$\SI{0}{\kelvin}$}
                & $5.1131$~\cite{ishiguroMagneticPropertiesHydrogen1954}
                    &
                        & $5.0650$~\cite{ishiguroMagneticPropertiesHydrogen1954}
                            & $5.0530$ $^\text{b}$~\cite{alijahHydrogenMoleculeH22019}
        \\
            & %empty
                & $5.08$~\cite{glickDiamagneticSusceptibilityGases1961}
                    &
                        &
                            & $4.952$ $^\text{b}$~\cite{ishiguroMagneticPropertiesHydrogen1954}
        \\
            & %empty
                & $5.04$ $^\text{c}$~\cite[p.~812]{jainDiamagneticSusceptibilityAnisotropy2007}
                    &
                        &
                            & $4.815$ $^\text{b}$~\cite{raynesNoteMagneticSusceptiblllty1974}
        \\
            & %empty
                & $5.01$ $^\text{c}$~\cite[p.~812]{jainDiamagneticSusceptibilityAnisotropy2007}
                    &
                        &
                            &
        \\
            & $\SI{300}{\kelvin}$
                & $5.080(3)$~\cite{ruudFullCICalculations1996}
                    &
                        &
                            &
        \\
        \tabfootnote{a}{Calculated with Rebane's method. Pair distance data is taken from indicated source.}\\
        \tabfootnote{b}{Isotropic susceptibility is obtained by calculating rotational average.}\\
        \tabfootnote{c}{Experimental value, temperature not available.}\\
    \end{tabular}
\end{table} %
\begin{figure}[!tb]
     \makebox[\textwidth][c]{
        \begin{subfigure}{.50\textwidth}
            \centering
            \includegraphics[width=1.0\textwidth]{figures/H2.pdf}
            \vspace*{-0.9cm}\caption{$\mathrm{H}_2$}
            \label{fig:susc12a}
        \end{subfigure}%
        %\hfill
        \begin{subfigure}{.50\textwidth}
            \centering
            \includegraphics[width=1.0\textwidth]{figures/Ps_Ps2.pdf}
            \vspace*{-0.7cm}\caption{$\mathrm{Ps}$ and $\mathrm{Ps}_2$}
            \label{fig:susc12b}
        \end{subfigure}
        $\qquad$$\quad$
     }
    \caption{Diamagnetic susceptibility of $\mathrm{H}_2$, $\mathrm{Ps}$ and $\mathrm{Ps}_2$ at various temperatures along with reference values~\cite{alexanderFullyNonadiabaticProperties2008, alexanderRovibrationallyAveragedProperties2007, alijahHydrogenMoleculeH22019, glickDiamagneticSusceptibilityGases1961, raynesNoteMagneticSusceptiblllty1974, jainDiamagneticSusceptibilityAnisotropy2007, ruudFullCICalculations1996, rebaneNonadiabaticTheoryDiamagnetic2002} from tables~\ref{tab:susc2} and~\ref{tab:susc3}.}
    \label{fig:susc12}
\end{figure}%
The susceptibilities of these systems increase with temperature, because the mean nuclear separation also increases due to centrifugal distortion caused by activating rotational states. The longer separation of the nuclei brings the system closer to the limit of two isolated atoms. The susceptibilities of each molecule are lower than twice the atomic susceptibilities, indicating that the bonding lowers the total susceptibility. Susceptibility of hydrogen molecule $\mathrm{H}_2$ is plotted in figure~\ref{fig:susc12a}. Note that PIMC results have lower margin of error than discrepancy of \SI{0}{\kelvin} AQ references. The difference between \SI{0}{\kelvin} AQ and BO values is caused by the zero-point vibration.

%\FloatBarrier
\subsection{Systems with low nuclear mass}
Susceptibilities of exotic systems $\mathrm{Ps}$ and $\mathrm{Ps}_2$ are presented in table~\ref{tab:susc3} and plotted in figure~\ref{fig:susc12b}. The susceptibility of positronium decreases at higher temperatures, unlike the rest of the monoatomic systems, which did not show notable thermal effects. Again, the susceptibility of dipositronium shows no statistically meaningful decrease with the temperature. Note that unlike $\mathrm{H}_2$, the susceptibility of $\mathrm{Ps}_2$ is greater than the susceptibility of two separate positroniums. For this reason, the susceptibility of $\mathrm{Ps}_2$ is expected to decrease at higher temperatures. Result of dipositronium differs considerably from Rebane's reference value, which is not a large concern, as Rebane's method has unclear precision for systems with multiple nuclei. The susceptibilities of $\mathrm{Ps}$ and $\mathrm{Ps}_2$ have not been studied at finite temperatures before. %The systems display how extreme nonadiabaticity affects on susceptibility.

Last, the effect of nuclear mass is inspected on diatomic molecules. Figure~\ref{fig:susc2} presents the susceptibilities of numerous artificial hydrogen-like molecules, which vary the nuclear mass on range from positron to eight times mass of a proton. The figure shows a smooth transition from dipositronium to the hydrogen molecule and beyond. An approximate shape of the data is presented by fitting a curve $\chi = am^{-1} + bm^{-\frac{1}{2}} + cm^{-\frac{1}{3}} + \chi_{\mathrm{BO}}$. This curve is only an approximation, and it is not a model reflecting the physical phenomenon. Concerning the temperature $T=\SI{1000}{\kelvin}$ and nuclear mass $m \geq 3 m_\mathrm{e}$, the coefficients are $a = -1.62\times 10^{-13}\frac{\mathrm{m}^3}{\mathrm{mol}\, \mathrm{m}_\mathrm{p}},\;b = 1.20\times 10^{-12}\frac{\mathrm{m}^3}{\mathrm{mol} \sqrt{\mathrm{m}_\mathrm{p}}}$ and $c = -2.83\times 10^{-12}\frac{\mathrm{m}^3}{\mathrm{mol} \sqrt[3]{\mathrm{m}_\mathrm{p}}}$. The error of the fit is less than 1\% for each data point.





% TODO table 6
\begin{table}[!tb]
    \centering
    \caption{Diamagnetic susceptibilities. The susceptibilities are in units of \suscUnit.}
    \label{tab:susc3}
    \begin{tabular}{ll|llll}
        $\chi$
            & $T$
                & $\mathrm{Ps}$
                    & $\mathrm{Ps}_2$
                        & $\mathrm{H}_2^+$ &\\
        \cline{1-5}
        \multirow{5}{*}{PIMC}
            & \SI{100}{\kelvin}
                & $23.77(6)$
                    & $49.29(25)$
                        &  &\\
            & \SI{300}{\kelvin}
                & $23.654(23)$
                    & $49.28(15)$
                        & $2.318(28)$ &\\
            & \SI{500}{\kelvin}
                &
                    & $49.14(14)$
                        & &\\
            & \SI{1000}{\kelvin}
                & $23.175(16)$
                    & ~$^\text{d}$
                        & $2.331(23)$ &\\
            & \SI{3000}{\kelvin}
                & $21.822(14)$
                    &
                        & ~$^\text{d}$ &\\
        \cline{1-5}
        Ref. %\multirow{1}{*}{Ref.}
            & \SI{0}{\kelvin}
                & $23.88663$ $^\text{a b}$
                    & $69.7449$ $^\text{a}$~\cite{bubinRelativisticCorrectionsGroundstate2007}
                        & $2.37944$ $^\text{c}$~\cite{cobaxinMathrmWeakMagnetic2015} &\\
        \tabfootnote{a}{Calculated with Rebane's method. Pair distance data is taken from indicated source.}\\ % It gives exact susceptibility if intermolecular distances $\ev{r^2}$ are known. Other indicated sources have provided $\ev{r^2}$ values.
        \tabfootnote{b}{Using analytical pair distance from equation~\ref{eq:r2integral}.}\\
        \tabfootnote{c}{BO calculation. Isotropic susceptibility is obtained by calculating rotational average.}\\
        \tabfootnote{d}{Nuclei are dissociated at this temperature.}\\
    \end{tabular}
    % Note that zero point energy for H$_2$ is $0.0074\; \cite{kylanpaaFiniteTemperatureQuantum2010a}
\end{table}

\begin{figure}[!tb]
     \makebox[\textwidth][c]{
         \centering
         \begin{subfigure}{.50\textwidth}
             \centering
             \includegraphics[width=1.0\textwidth]{figures/Ps2_varM.pdf}
             \vspace*{-0.9cm}\caption{Nucleus masses from $m_\mathrm{e}$ to $8m_\mathrm{p}$}
             \label{fig:susc2a}
         \end{subfigure}
         \begin{subfigure}{.50\textwidth}
             \centering
             \includegraphics[width=1.0\textwidth]{figures/H2_varM.pdf}
             \vspace*{-0.9cm}\caption{Nucleus masses from $\frac{m_\mathrm{p}}{8}$ to $8m_\mathrm{p}$}
             \label{fig:susc2b}
         \end{subfigure}%
     }
    \caption{Diamagnetic susceptibilities of hydrogen-like diatomic molecules, whose nuclear masses have been varied. The horizontal axis value $m = 1\,m_\mathrm{p}$ corresponds to the hydrogen molecule, $m = 2\,m_\mathrm{p}$ corresponds to the deuterium molecule, and $m = \frac{1}{1836}\,m_\mathrm{p}$ corresponds to the dipositronium. Figure (b) is a smaller section of figure (a). The reference values are the same as in table~\ref{tab:susc2}.}
    \label{fig:susc2}
\end{figure}


\section{Correlation considerations}

Susceptibilities of $\mathrm{Ps}$ and $\mathrm{Ps}_2$ are an order of magnitude higher than for other systems. This can be explained with the imaginary-time trajectories of the particles. The trajectories of each particle are spread out according to their thermal wave length, which is proportional to $\frac{1}{\sqrt{m T}}$. In case of the hydrogen atom, the trajectory of the nucleus is almost point-like compared to the large trajectory of the electron. However, in case of the positronium, the trajectory of nucleus has a size equal to the trajectory of the electron. The longer thermal wave length leads to an increased extent of the particle trajectory, which increases trajectory area, affecting the susceptibility.

The temperature-dependencies of $\mathrm{Ps}$ and $\mathrm{Ps}_2$ are interesting, as it shows that the temperatures can couple directly with the electronic structure if the nucleus is light enough. One possible explanation is that as the temperature rises, the imaginary-time trajectories shrink, but the mean particle distances remain constant. This causes the particle trajectories to have less overlap between each other. The PIMC calculation of positronium shows that at \SI{3000}{\kelvin} the positron is spread out on a scale that is comparable to interparticle distance ($\approx\SI{3}{\bohr}$), but at \SI{300}{\kelvin} the spread is an order of magnitude larger.

The susceptibility depends strongly on the correlation of the trajectories. This can be demonstrated with a system consisting of two particles that are denoted with $a$ and $b$. The susceptibility in equation~\ref{eq:finalSuscEqOfAll} can be decomposed as
\begin{align}
\chi &\propto \ev{\pa{q_a \ScalA_a + q_b \ScalA_b}^2}\\
&= q_a^2 \ev{\ScalA_a^2} + q_b^2 \ev{\ScalA_b^2} + 2q_a q_b \ev{\ScalA_a \ScalA_b}
\end{align}
where $q$ is the charge and $\ScalA$ is the area of the trajectory. Examples of two-electron systems are the positronium and the BO hydrogen molecule. In case of the positronium, $q_a q_b < 0$, and so the correlation term $\ev{\ScalA_a \ScalA_b}$ decreases susceptibility. In case of the BO hydrogen molecule, $q_a q_b > 0$, and so the correlation term increases susceptibility. However, there is no easy way to reason terms $\ev{\ScalA_a^2}$, $\ev{\ScalA_b^2}$ and $\ev{\ScalA_a \ScalA_b}$ intuitively. For example, it may appear intuitive, that shorter bond length of $\mathrm{H}_2$ increases the electron overlap, which would then increase the correlation term $\ev{\ScalA_a \ScalA_b}$. However, the correlation term of $\mathrm{H}_2$ is actually not affected, and the change in susceptibility is caused by terms $\ev{\ScalA_a^2}$ and $\ev{\ScalA_b^2}$. On the other hand, positronium shows strong changes in all three terms when the temperature is changed, which may be unexpected. So, the temperature dependency of the susceptibility depends on both the correlation and the thermal wavelength of the particle trajectories.



