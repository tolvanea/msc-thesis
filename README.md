# msc-thesis

Master of science thesis, Alpi Tolvanen

Subject: *Diamagnetic susceptibility with path integral Monte Carlo.*

University of Tampere, Computational Physics

Thesis instructors: Tapio Rantala, Juha Tiihonen

## PIMC results
Pimc values are calculated with (unpublished) pimc3-code using commit 2d9f5d6a. Systems Ps Ps2 and HD uses commit bbf24e7c. Private repository for code is
https://gitlab.com/compphys/est/pimc3 .
PIMC code can be obtained by contacting us.

All tables and figures are made with automatization tool
https://gitlab.com/tolvanea/PIMC-automatization .
Calculation templates are in directory `calculation_data`, and results should be somewhat replicable by following the instructions in `calculation_data/readme.md`. This directory also contains some extra results (tables and figures) for these systems.

## Getting pdf
Pre built pdf can be found from `aux/main.pdf`. 

Direct link to the pdf:https://gitlab.com/tolvanea/msc-thesis/-/raw/master/aux/main.pdf?inline=true

Pdf document can be compiled by running shell script `compile_latex` three times. On Ubuntu, all required dependencies should come with `texlive-full` package.

## Reference values
Reference values are calculated with script `codes/calculate_reference_values.py`. (The script indicates references with BibTex keys, which can be found from references.bib.)

## Presentation
Slide presentation of thesis can be found from
https://gitlab.com/tolvanea/msc-thesis-presentation

## Follow-up publication
Does the thesis not fit in your taste because it contains too much basic introduction and too many clarifying steps in derivation equations? No worries, we wrote a publication in Physical Review A, which compresses the 57 pages of diploma thesis into 10 pages, while also adding few more simulated systems! To be honest though, some of the derivation may be simpler in the publication, but you must already know the field to follow easily how one equation comes from another.

Read the final draft here: https://gitlab.com/tolvanea/thesis_publication/-/raw/master/final_draft.pdf?inline=true

Link to the paper (paywalled): https://journals.aps.org/pra/abstract/10.1103/PhysRevA.105.022816
